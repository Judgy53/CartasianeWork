package cartasiane.gui;

import java.util.List;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiMainMenu;
import net.minecraft.src.GuiScreen;
import cartasiane.misc.Grades;

public class GuiCredits extends GuiScreen
{

	GuiScreen parent;
	
    public GuiCredits(GuiScreen par1)
    {
    	this.parent = par1;
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
    	
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    protected void keyTyped(char c, int i)
    {
        if (i == 1)
        {
            mc.displayGuiScreen(parent);
        }
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        controlList.add(new GuiButton(1, width / 2 + 189, height/2 - 115, 20, 20, "X"));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton a)
    {
        if (a.id == 1)
        {
            mc.displayGuiScreen(parent);
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        drawDefaultBackground();
        
        drawRect(width / 2 - 213, height / 2 - 120, width/2 + 214, height/2 + 120, 0x20ffffff);
        drawRect(width / 2 - 212, height / 2 - 119, width/2 + 213, height/2 + 119, 0x90000000);
        
        drawString(fontRenderer, "\2473Client, serveur et services Cartasiane", width / 2 - 205, height / 2 - 110, 0xffffff);
        Grades grades = new Grades();
        
        t("Id�e originale:", width/2 - 205, height/2 - 90);
        p("MacTheZazou", width/2 - 205, height/2 - 75);
        
        t("Programmeurs:", width/2 - 205, height/2 - 60);
        for(int i = 0; i < grades.codeurs.length; i++)
        {
            p(grades.codeurs[i], width/2 - 205, height/2 - 45 + (i*10));
        }
        
        t("Architectes:", width/2 - 205, height/2 + 50);  
        for(int i = 0; i < grades.architectes.length; i++)
        {
            p(grades.architectes[i], width/2 - 205, height/2 + 62 + (i*10));
        }
        
        t("Webmasters:", width/2 - 110, height/2 - 90);
        for(int i = 0; i < grades.webmasters.length; i++)
        {
            p(grades.webmasters[i], width/2 - 110, height/2 - 75 + (i*10));
        }
        
        t("Graphistes:", width/2 - 110, height/2 - 40);
        for(int i = 0; i < grades.graphistes.length; i++)
        {
            p(grades.graphistes[i], width/2 - 110, height/2 - 25 + (i*10));
        }
        
        t("Partenaires:", width/2 - 110, height/2 + 50);
        for(int i = 0; i < grades.partenaires.length; i++)
        {
            p(grades.partenaires[i], width/2 - 110, height/2 + 65 + (i*10));
        }
        
        String s2 = "\2473Cartasiane alpha "+GuiMainMenu.cartaVer+" (build "+GuiMainMenu.cartaBuild+")";
        String s = "\2473Minecraft 1.4.5, Copyright Mojang AB";      
        drawString(fontRenderer, s, width/2 + 215 - fontRenderer.getStringWidth(s) - 10, height / 2 + 102, 0xffffff);
        drawString(fontRenderer, s2, width/2 + 215 - fontRenderer.getStringWidth(s2) - 10, height / 2 + 90, 0xffffff);
        
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(mc.renderEngine.getTexture("/carta/cartlogo.png"));
        drawTexturedModalRect(width/2 + 20, height/2 - 60, 0, 0, 256, 256);
        
        super.drawScreen(par1, par2, par3);
    }
    
    public void t(String a, int b, int c)
    {
        drawString(fontRenderer, "\2473"+a, b, c, 0xffffff);
    }
    
    public void p(String a, int b, int c)
    {
        drawString(fontRenderer, "\2476"+a.charAt(0) + "\247f"+a.substring(1), b, c, 0xffffff);
    }
}