package cartasiane.gui;

import org.lwjgl.opengl.GL11;

import java.awt.Scrollbar;
import java.lang.reflect.Field;
import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSlider;
import net.minecraft.src.GuiTextField;
import net.minecraft.src.StringTranslate;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList ;

import javax.swing.JSlider;


public class GuiFriendsList extends GuiScreen
{
    private EntityPlayer player;
    private int xSize;
    private int ySize;
    private GuiTextField Champ;
    private String newFriend;
    private ArrayList<String> amis = new ArrayList<String>();

    
    public GuiFriendsList(Minecraft minecraft, EntityPlayer entityplayer)
    {
        xSize = 127;
        ySize = 157;
        mc = minecraft;
        player = entityplayer;
    }
    //* Ouverture du Gui et r�cup�ration des amis dans le tableau dynamique amis
	public void Connexion() 
	{
		/*try
		{
			System.out.println("Ouverture liste d'amis et connexion");
    		Statement state = Connect.getInstance().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet result = state.executeQuery("SELECT joueur2 FROM amis WHERE joueur1 = '" + player.username + "' AND relation = 1");
			while (result.next())
			{
			    amis.add(result.getString("joueur2"));
			}
			
			
			result.close();
            state.close();


		} 
		catch (SQLException ex)
		{
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}*/
	}
	//* Fin interaction bdd

    
	
	
    public void initGui()
    {
    	Connexion();
        controlList.clear();
        int i = (width - xSize) / 2;
        int j = (height - ySize) / 2;
        this.Champ = new GuiTextField(this.fontRenderer, i + 9, j + 134, 90, 16);
        this.Champ.setText("Ajouter un ami");
        controlList.add(new GuiButton(0, i + 105, j + 132, 18, 20, "+"));
        
        controlList.add(new GuiButton(110, i - 20, j - 20, 20, 20, "R"));
    }
    
    public void drawScreen(int i, int j, float f)
    {
    	int zeroX = width/2 - 127/2;
    	int zeroY = height/2 - 157/2;
        this.drawDefaultBackground();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, mc.renderEngine.getTexture("/carta/guifriends.png"));
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        drawTexturedModalRect(zeroX, zeroY, 0, 0, 127, 157);
        
        drawString(fontRenderer, "Liste d'amis", zeroX + 8, zeroY + 14, 0xffffffff);
        Champ.drawTextBox();
        super.drawScreen(i, j, f);
    }
    
    protected void actionPerformed(GuiButton Button)
    {
    	if(Button.enabled)
    	{
    		if(Button.id == 110)
    		{
    			this.mc.displayGuiScreen(this);
    		}
    		
    		if(Button.id == 0)
    		{
    			boolean alreadyTaken = false;
    			int j = amis.size();
    			if(j != 0)
    			{
    				for(int i = 0; i < j; i++)
    				{
    					if(amis.get(i).equalsIgnoreCase(Champ.getText()))
    					{
    						alreadyTaken = true;
    					}
    				}
    			}
    			if(alreadyTaken)
    			{
    				this.drawCenteredString(this.fontRenderer, "Vous avez d�j� ajout� cet ami", this.width/2, 20, 16777215);
    			}
    			else
    			{
    				//* ajout d'un ami
    				/*try
    				{
    					System.out.println("Connexion pour ajouter un ami");
    		    		Statement state = Connect.getInstance().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
    					ResultSet result = state.executeQuery("SELECT nom FROM joueur WHERE nom = '"+ Champ.getText() + "'");
    					if(!result.next())
    					{
    	    				this.drawCenteredString(this.fontRenderer, "Ce joueur n'existe pas", this.width/2, 20, 16777215);
    					}
    					else
    					{
    						result = state.executeQuery("SELECT * FROM amis");
    						result.last();
    						if (!result.next())
    						{
    							amis.add(Champ.getText());
    							result.moveToInsertRow();
    							result.updateString("joueur1", player.username);
    							result.updateString("joueur2", Champ.getText());
    							result.updateString("relation", "1");
    							result.insertRow();
    		    				this.drawCenteredString(this.fontRenderer, Champ.getText() + "a �t� ajout�", this.width/2, 20, 16777215);
    						}
    					}
    					result.close();
    		            state.close();

    				}
    				catch(SQLException ex){
    					System.out.println("SQLException: " + ex.getMessage());
    					System.out.println("SQLState: " + ex.getSQLState());
    					System.out.println("VendorError: " + ex.getErrorCode());
    				}*/
    			}
    		}
    	}
    }
    
    protected void mouseClicked(int i, int j, int k)
    {
    	Champ.mouseClicked(i, j, k);
    	if(Champ.isFocused())
    	{
    		Champ.setText("");
    	}
        super.mouseClicked(i, j, k);
    }
    

    protected void keyTyped(char c, int i)
    {
    	Champ.textboxKeyTyped(c, i);
        super.keyTyped(c, i);
    }
    
    public void updateScreen()
    {
        super.updateScreen();
        Champ.updateCursorCounter();
    }
}