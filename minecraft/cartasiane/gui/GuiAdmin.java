package cartasiane.gui;

import java.util.List;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiPlayerInfo;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;
import net.minecraft.src.NetClientHandler;
import net.minecraft.src.ScaledResolution;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiAdmin extends GuiScreen
{
    String selectedPlayer = "Aucun";
    
    private GuiTextField textboxReason;
    
    GuiButton pgPrev, pgFor;

    public GuiAdmin()
    {
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
    	textboxReason.updateCursorCounter();
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
    	Keyboard.enableRepeatEvents(true);
    	ScaledResolution scaledresolution = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
        int i = scaledresolution.getScaledWidth();
        int j = scaledresolution.getScaledHeight();
    	 mc.mcProfiler.startSection("playerList");
         NetClientHandler netclienthandler = mc.thePlayer.sendQueue;
         java.util.List list = netclienthandler.playerInfoList;
         //int i3 = netclienthandler.currentServerMaxPlayers;
         int i3 = 80;
         int k3 = i3;
         int i4 = 1;

         for (; k3 > 20; k3 = ((i3 + i4) - 1) / i4)
         {
             i4++;
         }

         int k4 = 300 / i4;

         if (k4 > 150)
         {
             k4 = 150;
         }

         int j5 = (i - i4 * k4) / 2;
         byte byte0 = 10;

         for (int l6 = 0; l6 < i3; l6++)
         {
             int k7 = j5 + (l6 % i4) * k4;
             int j8 = byte0 + (l6 / i4) * 9;
             GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
             GL11.glEnable(GL11.GL_ALPHA_TEST);

             if (l6 >= list.size())
             {
                 continue;
             }

             GuiPlayerInfo guiplayerinfo = (GuiPlayerInfo)list.get(l6);
             controlList.add(new GuiButton(10, k7 - 4, j8 - 1, k4, 10, guiplayerinfo.name, "noback"));
         }
         
         int a = width/2 - 150;
         int b = (byte0 + 9 * k3) + byte0 + 16;
         
         controlList.add(new GuiButton(1, a + 234, b + 2, 65, 20, "Refresh"));
         controlList.add(new GuiButton(2, a + 5, b + 28, 40, 20, "Kick"));
         controlList.add(new GuiButton(3, a + 46, b + 28, 40, 20, "Ban"));
         controlList.add(pgPrev = new GuiButton(4, a + 254, b + 31, 14, 20, "<"));
         controlList.add(pgFor = new GuiButton(5, a + 284, b + 31, 14, 20, ">"));
         textboxReason = new GuiTextField(fontRenderer, a + 90, b + 29, 100, 18);
         textboxReason.setMaxStringLength(20);
     	 textboxReason.setText("Raison");
     	 pgPrev.enabled = false;
    }

    /**
     * Called when the screen is unloaded. Used to disable keyboard repeat events
     */
    public void onGuiClosed()
    {
        Keyboard.enableRepeatEvents(false);
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton par1GuiButton)
    {
        if (par1GuiButton.id == 1)
        {
            mc.displayGuiScreen(null);
            mc.displayGuiScreen(new GuiAdmin());
        }
        
	    if (par1GuiButton.id == 10)
	    {
	    	selectedPlayer = par1GuiButton.displayString;
	    }
        
        if (par1GuiButton.id == 2)
        {
            if(selectedPlayer == "Aucun" || textboxReason.getText() == "Raison")
            {
            	mc.thePlayer.addChatMessage("Vous n'avez pas selectioné de joueur et/ou de raison.");
            	return;
            }
            mc.thePlayer.sendChatMessage("/kick "+selectedPlayer+" "+textboxReason.getText());
        }
        
        if (par1GuiButton.id == 3)
        {
            if(selectedPlayer == "Aucun" || textboxReason.getText() == "Raison")
            {
            	mc.thePlayer.addChatMessage("Vous n'avez pas selectioné de joueur et/ou de raison.");
            	return;
            }
            mc.thePlayer.sendChatMessage("/ban "+selectedPlayer+" "+textboxReason.getText());
        }
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    protected void keyTyped(char par1, int par2)
    {
    	textboxReason.textboxKeyTyped(par1, par2);
    	
	    if (par2 == 1)
	    {
	    	mc.displayGuiScreen(null);
	    }
    }

    /**
     * Called when the mouse is clicked.
     */
    protected void mouseClicked(int par1, int par2, int par3)
    {
        super.mouseClicked(par1, par2, par3); 
        textboxReason.mouseClicked(par1, par2, par3); 
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {	
         ScaledResolution scaledresolution = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
         int i = scaledresolution.getScaledWidth();
         int j = scaledresolution.getScaledHeight();
    	 mc.mcProfiler.startSection("playerList");
         NetClientHandler netclienthandler = mc.thePlayer.sendQueue;
         java.util.List list = netclienthandler.playerInfoList;
         //int i3 = netclienthandler.currentServerMaxPlayers;
         int i3 = 80;
         int k3 = i3;
         int i4 = 1;

         for (; k3 > 20; k3 = ((i3 + i4) - 1) / i4)
         {
             i4++;
         }

         int k4 = 300 / i4;

         if (k4 > 150)
         {
             k4 = 150;
         }

         int j5 = (i - i4 * k4) / 2;
         byte byte0 = 10;
         drawRect(j5 - 1, byte0 - 1, j5 + k4 * i4, byte0 + 9 * k3, 0xee000000);

         for (int l6 = 0; l6 < i3; l6++)
         {
             int k7 = j5 + (l6 % i4) * k4;
             int j8 = byte0 + (l6 / i4) * 9;
             drawRect(k7, j8, (k7 + k4) - 1, j8 + 8, 0x20ffffff);
             GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
             GL11.glEnable(GL11.GL_ALPHA_TEST);

             if (l6 >= list.size())
             {
                 continue;
             }

             GuiPlayerInfo guiplayerinfo = (GuiPlayerInfo)list.get(l6);
             mc.renderEngine.bindTexture(mc.renderEngine.getTexture("/gui/icons.png"));
             int i10 = 0;
             byte byte1 = 0;

             if (guiplayerinfo.responseTime < 0)
             {
                 byte1 = 5;
             }
             else if (guiplayerinfo.responseTime < 150)
             {
                 byte1 = 0;
             }
             else if (guiplayerinfo.responseTime < 300)
             {
                 byte1 = 1;
             }
             else if (guiplayerinfo.responseTime < 600)
             {
                 byte1 = 2;
             }
             else if (guiplayerinfo.responseTime < 1000)
             {
                 byte1 = 3;
             }
             else
             {
                 byte1 = 4;
             }
             
             zLevel += 100F;
             drawTexturedModalRect((k7 + k4) - 12, j8, 0 + i10 * 10, 176 + byte1 * 8, 10, 8);
             zLevel -= 100F;
         }
         
         int a = width/2 - 150;
         int b = (byte0 + 9 * k3) + byte0 + 15;
         int c = (byte0 + 9 * k3) + 80;
         drawRect(width/2 - 150 -1, (byte0 + 9 * k3) + byte0 - 1, width/2 + 150 + 1, c, 0xee000000);
         drawRect(width/2 - 150, (byte0 + 9 * k3) + byte0, width/2 + 150, c - 1, 0x20ffffff);
         drawHorizontalLine(width/2 - 150, width/2 + 150, b, 0xcc000000);
    	 textboxReason.drawTextBox();
         
         fontRenderer.drawString("\247eAdministration", a + 5, b - 11, 0xffffff);
         String s = "\2474"+mc.session.username;
         fontRenderer.drawString(s, width/2 + 150 - fontRenderer.getStringWidth(s) - 2, b - 11, 0xffffff);
         fontRenderer.drawString("Joueur séléctionné: "+selectedPlayer, a + 5, b + 10, 0xffffff);
         fontRenderer.drawString("1", a + 273, b + 38, 0xffffff);

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glDisable(GL11.GL_LIGHTING);
         GL11.glEnable(GL11.GL_ALPHA_TEST);
     
    	super.drawScreen(par1, par2, par3);
    }
    
    public boolean doesGuiPauseGame()
    {
    	return false;
    } 
}
