package cartasiane.net;

import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.GuiDownloadTerrain;
import net.minecraft.src.IntegratedServer;
import net.minecraft.src.NetClientHandler;
import net.minecraft.src.Packet1Login;
import net.minecraft.src.PlayerControllerMP;
import net.minecraft.src.StatList;
import net.minecraft.src.WorldClient;
import net.minecraft.src.WorldSettings;

public class NetCartaHandler extends NetClientHandler
{
	public void handleLogin(Packet1Login par1Packet1Login)
    {
        this.mc.playerController = new PlayerControllerMP(this.mc, this);
        this.mc.statFileWriter.readStat(StatList.joinMultiplayerStat, 1);
        this.worldClient = new WorldClient(this, new WorldSettings(0L, par1Packet1Login.gameType, false, par1Packet1Login.hardcoreMode, par1Packet1Login.terrainType), par1Packet1Login.dimension, par1Packet1Login.difficultySetting, this.mc.mcProfiler);
        this.worldClient.isRemote = true;
        this.mc.loadWorld(this.worldClient);
        this.mc.thePlayer.dimension = par1Packet1Login.dimension;
        this.mc.displayGuiScreen(new GuiDownloadTerrain(this));
        this.mc.thePlayer.entityId = par1Packet1Login.clientEntityId;
        this.currentServerMaxPlayers = par1Packet1Login.maxPlayers;
        this.mc.playerController.setGameType(par1Packet1Login.gameType);
        this.mc.gameSettings.sendSettingsToServer();
    }

	public NetCartaHandler(Minecraft par1Minecraft, String par2Str, int par3)
			throws IOException {
		super(par1Minecraft, par2Str, par3);
	}
	
	public NetCartaHandler(Minecraft par1Minecraft,
			IntegratedServer par2IntegratedServer) throws IOException {
		super(par1Minecraft, par2IntegratedServer);
	}
	
	public void handleStartAnim(Packet157StartAnim packet)
	{
		EntityPlayer player = (EntityPlayer)getEntityByID(packet.playerId);
		player.setAnimation(mc.animLoader.getAnim(packet.animId), packet.length, packet.left);
	}

}
