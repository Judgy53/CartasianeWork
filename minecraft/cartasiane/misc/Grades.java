package cartasiane.misc;

import net.minecraft.client.Minecraft;

public class Grades 
{
	public static String admins[] = 
	{
		"MacTheZazou",
		"Trau18", 
		"Kronos"
	};
	
    public static String codeurs[] = 
    {
    	"doubi125", 
    	"Epharos", 
    	"JudgeDead_53",
    	"Kronos", 
    	"MacTheZazou",
    	"Sangrene",
    	"Splliter",
    	"On3",
    	"Zion_Livity"
    };
    
    public static String architectes[] = 
    {
    	"Magistiquel",
    	"MisterSatch", 
    	"Nighty76000", 
    	"TheLastGhost"
    };
    
    public static String webmasters[] = 
    {
    	"Kronos",
    	"Ti-Biscuit",
    	"Yahochi"
    };
    
    public static String graphistes[] =
    {
    	"Abclive",
    	"Airooh", 
    	"Lila", 
    	"Maxigregrze",
    	"Nerpson",
    	"Tom",
    	"Yahochi"
    };
    
    public static String partenaires[] = 
    {
    	"Ironcraft"
    };
}