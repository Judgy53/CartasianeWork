package cartasiane.misc;

public class Splashs
{
	
    public static String getRandomSplash()
    {
		return splash[(int) (Math.random() * splash.length)];   	
    }
    
    public static String splash[] =
    {
    	"La vie tranquille !",
    	"Le 1er serveur MMORPG francophone !",
    	"Bon jeu !",
    	"Gloire � Cartasiane !",
    	"Vive la communaut� !",
    	"Longue vie au serveur !",
    	"Les Orcs sont un peu volumineux, non ?",
    	"www.cartasiane.com!",
    	"Ne crois en personne � part toi.",
    	"Mieux que le RP, voici le MMORPG sur Minecraft !",
    	"Notch n'a qu'a bien se tenir !",
    	"Non, rien de rien.",
    	"Lass� de votre vieux Minecraft ?",
    	"Whoua ! Des boutons lisses et rouge sang !",
    	"Votre imagination pour seule limite",
    	"Il est gentil le chien-chien !",
    	"Vous... ne passerez... pas !",
    	"Que je tr�passe si je faiblis !",
    	"- Une sph�re. - Une quoi ?",
    	"Peur de reprendre votre vieux Minecraft ?",
    	"R�veiller un nain pendant sa sieste ? Mauvaise id�e...",
    	"Petit poney",
    	"Poney Kawai <3",
    	"Interdit au moins que rien !",
    	"Toutes les news sur le site web !",
    	"Un cube n'est qu'un composant essentiel de la vie.",
    	"C'est en agissant que l'on apprend.",
    	"N'essayez pas de cheater, c'est impossible.",
    	"Xray : pratique mais incompatible ici.",
    	"Salet� de mosquito ! Tuez le !",
    	"Ta vie est dure ? Quelle vie ?",
    	"G33K SP0TT3D !",
    	"Ce client est 3 fois plus lourd que l'original.",
    	"280 Original !!",
    	"Riiiight ?",
    	"Kronos, le seul brony de l'�quipe!"
    };
}