package cartasiane.util;

import java.util.ArrayList;

import net.minecraft.src.Tessellator;

import org.lwjgl.opengl.GL11;

public class Bezier
{
	private static int precision = 50;
	public Bezier(float x1, float y1, float x2, float y2, float xv1, float yv1,
			float xv2, float yv2)
	{
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.xv1 = xv1;
		this.yv1 = yv1;
		this.xv2 = xv2;
		this.yv2 = yv2;
	}
	
	public void draw(Tessellator t, float xc, float yc, float xOrigin, float yOrigin, float scaleX, float scaleY, float w, float h, float r, float g, float b, int width)
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	    GL11.glEnable(GL11.GL_BLEND);
	    GL11.glDisable(GL11.GL_ALPHA_TEST);
	    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	    GL11.glShadeModel(GL11.GL_SMOOTH);
        GL11.glColor3f(r, g, b);
        GL11.glLineWidth(width);
		t.startDrawing(3);
		
		float xpt1 = x1*w + xOrigin;
		float xpt2 = x2*w + xOrigin;
		float ypt1 = yOrigin - h/2 + y1*h;
		float ypt2 = yOrigin - h/2 + y2*h;
		precision = 50;
		float lastXpt = xpt1;
		float lastYpt = ypt1;
		for(int i=1; i<precision; i++)
		{
			float ratio = i/(float)precision;
			float xv3 = Math.min(xv1*3*w, Math.abs(xpt2-xpt1));
			float xv4 = Math.max(Math.min(xv2*3*w, Math.abs(xpt2-xpt1)), -Math.abs(xpt2-xpt1));
			float xG1 = bary(xpt1, xpt1+xv3, ratio);
			float yG1 = bary(ypt1, ypt1+yv1*h, ratio);
			float xG2 = bary(xpt2+xv4, xpt2, ratio);
			float yG2 = bary(ypt2+yv2*h, ypt2, ratio);
			
			float xpt = bary(xG1, xG2, ratio), ypt = bary(yG1, yG2, ratio);
			drawClippedLine(t, lastXpt*scaleX + xc, lastYpt*scaleY + yc + h/2, xpt*scaleX + xc, ypt*scaleY + yc + h/2, xc, yc, w, h);
			lastXpt = xpt;
			lastYpt = ypt;
		}
		drawClippedLine(t, lastXpt*scaleX + xc, lastYpt*scaleY + yc + h/2, xpt2*scaleX + xc, ypt2*scaleY + yc + h/2, xc, yc, w, h);
		
        t.draw();
        GL11.glColor3f(1, 0, 0);
		t.startDrawing(3);
		drawClippedLine(t, xpt1*scaleX + xc, ypt1*scaleY + yc + h/2, (xpt1 + xv1*w)*scaleX + xc, (ypt1+yv1*h)*scaleY + yc + h/2, xc, yc, w, h);
        t.draw();
        t.startDrawing(3);
		drawClippedLine(t, xpt2*scaleX + xc, ypt2*scaleY + yc + h/2, (xpt2 + xv2*w)*scaleX + xc, (ypt2+yv2*h)*scaleY + yc + h/2, xc, yc, w, h);
        t.draw();
        if(xpt2+xv2*w >= 0 && xpt2+xv2*w <= w && (ypt2+yv2*h)*scaleY + h/2 >= 0 && (ypt2+yv2*h)*scaleY + h/2 <= h)
        {
        	t.startDrawingQuads();
        	t.addVertex((xpt2-1+xv2*w)*scaleX + xc, (ypt2+yv2*h)*scaleY + yc + h/2 - 1, 0);
        	t.addVertex((xpt2-1+xv2*w)*scaleX + xc, (ypt2+yv2*h)*scaleY + yc + h/2 + 1, 0);
        	t.addVertex((xpt2+1+xv2*w)*scaleX + xc, (ypt2+yv2*h)*scaleY + yc + h/2 + 1, 0);
        	t.addVertex((xpt2+1+xv2*w)*scaleX + xc, (ypt2+yv2*h)*scaleY + yc + h/2 - 1, 0);
        	t.draw();
        }
        
        if(xpt1+xv1*w >= 0 && xpt1+xv1*w <= w && (ypt1+yv1*h)*scaleY + h/2 >= 0 && (ypt1+yv1*h)*scaleY + h/2 <= h)
        {
			t.startDrawingQuads();
	        t.addVertex((xpt1+xv1*w)*scaleX + xc-1, (ypt1+yv1*h)*scaleY + yc + h/2 - 1, 0);
	        t.addVertex((xpt1+xv1*w)*scaleX + xc-1, (ypt1+yv1*h)*scaleY + yc + h/2 + 1, 0);
	        t.addVertex((xpt1+xv1*w)*scaleX + xc+1, (ypt1+yv1*h)*scaleY + yc + h/2 + 1, 0);
	        t.addVertex((xpt1+xv1*w)*scaleX + xc+1, (ypt1+yv1*h)*scaleY + yc + h/2 - 1, 0);
	        t.draw();
        }
        GL11.glColor3f(0, 1, 1);
        GL11.glLineWidth(3);
//        if(xpt2 >= xc && xpt2 <= xc+w && ypt2 >= yc && ypt2 <= yc+h)
        {
			t.startDrawingQuads();
	        t.addVertex(xpt2*scaleX + xc - 1, ypt2*scaleY + yc + h/2 - 1, 0);
	        t.addVertex(xpt2*scaleX + xc - 1, ypt2*scaleY + yc + h/2 + 1, 0);
	        t.addVertex(xpt2*scaleX + xc + 1, ypt2*scaleY + yc + h/2 + 1, 0);
	        t.addVertex(xpt2*scaleX + xc + 1, ypt2*scaleY + yc + h/2 - 1, 0);
	        t.draw();
        }
        GL11.glLineWidth(width);
        GL11.glColor3f(1, 1, 1);
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
	
	private void drawClippedLine(Tessellator t, float x1, float y1, float x2, float y2, float x, float y, float w, float h)
	{
		float xInters, yInters, ratio;
		if(x1 >= x && x1 <= x+w && y1 >= y && y1 <= y+h)
			t.addVertex(x1, y1, 0);
		if(x2 >= x && x2 <= x+w && y2 >= y && y2 <= y+h)
			t.addVertex(x2, y2, 0);
		ratio = (x-x1)/(x2-x1);
		yInters = y1 + (y2-y1)*ratio;
		if(ratio >= 0 && ratio <= 1 && yInters >= y && yInters <= y+h)
			t.addVertex(x, yInters, 0);
		
		ratio = (y-y1)/(y2-y1);
		xInters = x1 + (x2-x1)*ratio;
		if(ratio >= 0 && ratio <= 1 && xInters >= x && xInters <= x+w)
			t.addVertex(xInters, y, 0);
		
		ratio = (x+w-x1)/(x2-x1);
		yInters = y1 + (y2-y1)*ratio;
		if(ratio >= 0 && ratio <= 1 && yInters >= y && yInters <= y+h)
			t.addVertex(x+w, yInters, 0);
		
		ratio = (y+h-y1)/(y2-y1);
		xInters = x1 + (x2-x1)/(y2-y1)*(y+h-y1);
		if(ratio >= 0 && ratio <= 1 && xInters >= x && xInters <= x+w)
			t.addVertex(xInters, y+h, 0);
	}
	
	private float bary(float x1, float x2, float ratio)
	{
		return (1-ratio)*x1+ratio*x2;
	}
	
	void getFunction(ArrayList<AnimPoint> points)
	{
		float xpt1 = x1;
		float xpt2 = x2;
		float ypt1 = y1;
		float ypt2 = y2;
		precision = 50;
		float lastXpt = xpt1;
		float lastYpt = ypt1;
		for(int i=1; i<precision; i++)
		{
			float ratio = i/(float)precision;
			float xv3 = Math.min(xv1*3, Math.abs(xpt2-xpt1));
			float xv4 = Math.max(Math.min(xv2*3, Math.abs(xpt2-xpt1)), -Math.abs(xpt2-xpt1));
			float xG1 = bary(xpt1, xpt1+xv3, ratio);
			float yG1 = bary(ypt1, ypt1+yv1, ratio);
			float xG2 = bary(xpt2+xv4, xpt2, ratio);
			float yG2 = bary(ypt2+yv2, ypt2, ratio);
			
			float xpt = bary(xG1, xG2, ratio), ypt = bary(yG1, yG2, ratio);
			points.add(new AnimPoint(xpt, ypt));
			lastXpt = xpt;
			lastYpt = ypt;
		}
		points.add(new AnimPoint(xpt2, ypt2));
		points.add(new AnimPoint(xpt1, ypt1));
	}
	
	
	float x1, y1, x2, y2, xv1, yv1, xv2, yv2;

}
