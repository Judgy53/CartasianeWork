package cartasiane.util;

import net.minecraft.src.NBTTagCompound;

public class AnimPoint
{
	public AnimPoint(float time, float var)
	{
		this.time = time;
		this.var = var;
	}
	
	public AnimPoint(NBTTagCompound tag)
	{
		this.time = tag.getFloat("time");
		this.var = tag.getFloat("var");
	}
	
	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setFloat("time", time);
		tag.setFloat("var", var);
		return tag;
	}
	
	public float time, var;
}