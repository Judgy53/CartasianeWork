package cartasiane.util;

import net.minecraft.src.GuiScreen;
import net.minecraft.src.Tessellator;

import org.lwjgl.opengl.GL11;

public class GuiFunctionDrawer extends GuiScreen
{
	BezierCurve f = new BezierCurve();
	boolean click = false;
	public GuiFunctionDrawer()
	{
		click = false;
	}
	
	protected void mouseClicked(int par1, int par2, int par3)
	{
		click = true;
		if(par3 == 0)
			f.addPoint(par1, par2);
		else if(par3 == 1)
			f.selectPoint(par1, par2);
		else
			f.delPoint(par1, par2);
	}
	
	protected void mouseMovedOrUp(int par1, int par2, int par3)
    {
        if(par3 == 1)
        	f.unselect();
    }
	
	public void drawScreen(int par1, int par2, float par3)
    {
		f.movePoint(par1, par2);
        this.drawDefaultBackground();
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        Tessellator var9 = Tessellator.instance;

//    	f.draw(Tessellator.instance);
        
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glPushMatrix();
//        mc.entityRenderer.renderWorldbis(0.2771834f, 39407776701856l);
        GL11.glPopMatrix();

    }
}
