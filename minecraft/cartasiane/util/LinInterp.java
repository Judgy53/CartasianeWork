package cartasiane.util;

import java.util.ArrayList;
import java.util.Vector;

import net.minecraft.src.MathHelper;
import net.minecraft.src.Tessellator;

import org.lwjgl.opengl.GL11;

public class LinInterp extends Function
{
	int selectedPoint = -1;
	public LinInterp()
	{
		this.x = new Vector<Float>();
		this.y = new Vector<Float>();
	}
	
	public LinInterp(Vector<Float> x, Vector<Float> y)
	{
		this.x = new Vector<Float>();
		this.y = new Vector<Float>();
		for(int i=0; i<x.size(); i++)
		{
			int j;
			for(j=0; j < this.x.size() && x.get(i) > this.x.get(j); j++);
			this.x.insertElementAt(x.get(i), j);
			this.y.insertElementAt(y.get(i), j);
		}
	}

	public LinInterp(Vector<AnimPoint> l)
	{
		this.x = new Vector<Float>();
		this.y = new Vector<Float>();
		for(int i=0; i<l.size(); i++)
		{
			int j;
			for(j=0; j < this.x.size() && l.get(i).time > this.x.get(j); j++);
			this.x.insertElementAt(l.get(i).time, j);
			this.y.insertElementAt(l.get(i).var, j);
		}
	}

	public void addkey(float x, float y)
	{
		int j;
		for(j=0; j < this.x.size() && x > this.x.get(j); j++);
		this.x.insertElementAt(x, j);
		this.y.insertElementAt(y, j);
	}

	public float calc(float x)
	{
		int i=0;
		for(i=0; i < this.x.size()-1 && this.x.get(i) < x; i++);
		if(y.size() <=2 || y.size() <=2)
			return 0;
		if(i==0)
			return this.y.get(0) + (x - this.x.get(0))/(this.x.get(1) - this.x.get(0))*(this.y.get(1) - this.y.get(0));
		return this.y.get(i-1) + (x - this.x.get(i-1))/(this.x.get(i) - this.x.get(i-1))*(this.y.get(i) - this.y.get(i-1));
	}
	
	public void draw(Tessellator t)
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	    GL11.glEnable(GL11.GL_BLEND);
	    GL11.glDisable(GL11.GL_ALPHA_TEST);
	    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	    GL11.glShadeModel(GL11.GL_SMOOTH);
		t.startDrawing(3);
		t.addVertex(0, calc(0), 0);
		for(int i=0; i<x.size(); i++)
			t.addVertex(x.get(i), y.get(i), 0);
		t.addVertex(800, calc(800), 0);
        t.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
	
	
	
	Vector<Float> x, y;
}
