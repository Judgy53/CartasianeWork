package cartasiane.mobs;
import net.minecraft.src.Entity;
import net.minecraft.src.MathHelper;
import net.minecraft.src.ModelBase;
import net.minecraft.src.ModelRenderer;

public class ModelMagicFire extends ModelBase
{
  //fields
    ModelRenderer p1;
    ModelRenderer p2;
    ModelRenderer p3;
    ModelRenderer p4;
    public float size = 0.5F;
  public ModelMagicFire()
  {
    textureWidth = 64;
    textureHeight = 32;
      p1 = new ModelRenderer(this, 0, 0);
      p1.addBox(0F, 0F, 0F, 3, 3, 3);
      p1.setRotationPoint(0F, 0F, 0F);
      p1.setTextureSize(64, 32);
      p1.mirror = true;
      setRotation(p1, 0F, 0F, 0F);
      p2 = new ModelRenderer(this, 0, 0);
      p2.addBox(0F, 0F, 0F, 3, 3, 3);
      p2.setRotationPoint(-0.5333334F, 0F, 1.533333F);
      p2.setTextureSize(64, 32);
      p2.mirror = true;
      setRotation(p2, 0F, 0.7853982F, 0F);
      p3 = new ModelRenderer(this, 0, 0);
      p3.addBox(0F, 0F, 0F, 3, 3, 3);
      p3.setRotationPoint(1.6F, -0.4666667F, 3F);
      p3.setTextureSize(64, 32);
      p3.mirror = true;
      setRotation(p3, -0.7853982F, 1.570796F, 0F);
      p4 = new ModelRenderer(this, 0, 0);
      p4.addBox(0F, 0F, 0F, 3, 3, 3);
      p4.setRotationPoint(0F, -0.4666667F, 1.466667F);
      p4.setTextureSize(64, 32);
      p4.mirror = true;
      setRotation(p4, -0.7853982F, 0F, 0F);
  }
  public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
  {
    super.render(par1Entity, par2, par3, par4, par5, par6, par7);
    this.setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
    p1.render(par7);
    p2.render(par7);
    p3.render(par7);
    p4.render(par7);
  } 
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  } 
  public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
  { 
  }
}
