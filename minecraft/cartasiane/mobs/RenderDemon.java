package cartasiane.mobs;

import org.lwjgl.opengl.GL11;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.ModelBase;
import net.minecraft.src.RenderLiving;

public class RenderDemon extends RenderLiving
{

	public RenderDemon(ModelBase par1ModelBase, float par2) {
		super(par1ModelBase, par2);	
	}
	 public void RenderDemon(EntityDemon par1EntityDemon, double par2, double par4, double par6, float par8, float par9)
	    {
	        super.doRenderLiving(par1EntityDemon, par2, par4, par6, par8, par9);
	    }
	    public void doRenderLiving(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9)
	    {
	        this.RenderDemon((EntityDemon)par1EntityLiving, par2, par4, par6, par8, par9);
	    }
	    public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9) {
	    	this.RenderDemon((EntityDemon)par1Entity, par2, par4, par6, par8, par9);
	   }
}
