package cartasiane.mobs;
import net.minecraft.src.Entity;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Render;
import org.lwjgl.opengl.GL11;

public class RenderMagicFire extends Render {
	
	private ModelMagicFire mdl;
	private float size;
	
	public RenderMagicFire(ModelMagicFire model, float f)
	{
		this.mdl = model;
		this.size = f;
	}
	
	public void doRenderMagicFire(EntityMagicFire entitymf,	double d, double d1, double d2, float f, float f1) 
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float) d, (float) d1, (float) d2);
		GL11.glEnable(32826);
		this.loadTexture("/carta/mobs/boule.png");
		float f2 = this.size;
		GL11.glRotatef(30F, 1.0F, 0F, 0F);
		mdl.render(entitymf,(float) d, (float) d1, (float) d2, f, f1, f2);
		GL11.glDisable(32826);
		GL11.glPopMatrix();
	}
	
	public void doRender(Entity entity, double d, double d1, double d2, float f, float f1)
	{
		doRenderMagicFire((EntityMagicFire) entity, d, d1, d2, f, f1);
	}
}