package cartasiane.mobs;

import java.util.Random;


import net.minecraft.src.*;

public class EntityDemon extends EntityMob
{
    private Entity targetedEntity = null;
    private float heightOffset = 0.5F;
    private int heightOffsetUpdateTime;
    private int aggroCooldown = 0;
    public int attackCounter = 0;
	private double targetedPosX;
	private double targetedPosY;
	private double targetedPosZ;

    public EntityDemon(World par1World)
    {
        super(par1World);
        this.texture = "/carta/mobs/demon.png";      		    
        this.isImmuneToFire = true;             	
        this.experienceValue = 5;              							
    	this.moveSpeed = 0.3F;
    	this.attackCounter = 0;
        this.setSize(0.5F, 0.8F);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(2, new EntityAIAttackOnCollide(this, EntityPlayer.class, this.moveSpeed, false));
        this.tasks.addTask(6, new EntityAIWander(this, this.moveSpeed));
        this.tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(7, new EntityAILookIdle(this));
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 16.0F, 0, true));
    }
    public int getAttackStrength(Entity par1Entity)
    {
        return 6;
    }
    
    public boolean isAIEnabled()
    {
    	return true; 
    }
    
    public int getMaxHealth()
    {
        return 20;           	
    }
    
    public void onLivingUpdate()
    {
    	super.onLivingUpdate();   
    	if (this.targetedEntity != null && this.targetedEntity.isDead)
        {
            this.targetedEntity = null;
        }
        if (this.targetedEntity == null || this.aggroCooldown-- <= 0)
        {
            this.targetedEntity = this.worldObj.getClosestVulnerablePlayerToEntity(this, 100.0D);

            if (this.targetedEntity != null)
            {
                this.aggroCooldown = 20;
            }
        }    
        double var9 = 64.0D;
    	if (this.targetedEntity != null && this.targetedEntity.getDistanceSqToEntity(this) < var9 * var9)
        {
        	targetedPosX = (this.targetedEntity.posX - this.posX)/10;
        	targetedPosY = (this.targetedEntity.boundingBox.minY + (double)(this.targetedEntity.height / 2.0F) - (this.posY + (double)(this.height / 2.0F)))/10;
        	targetedPosZ = (this.targetedEntity.posZ - this.posZ)/10;
            this.renderYawOffset = this.rotationYaw = -((float)Math.atan2(targetedPosX, targetedPosZ)) * 180.0F / (float)Math.PI;
            if (this.canEntityBeSeen(this.targetedEntity))
	        {
	            ++this.attackCounter;
	            if (this.attackCounter > 0 && this.attackCounter < 50)
	            {
	            	
	            	  this.worldObj.spawnParticle("largesmoke", this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D);
	            	  this.worldObj.spawnParticle("flame", this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D);
	            }
	            else if (this.attackCounter > 50 && this.attackCounter < 54)
	            {
	                EntityMagicFire var2 = new EntityMagicFire(this.worldObj, this);
	                double var3 = targetedEntity.posX - this.posX;
	                double var5 = targetedEntity.posY + (double)targetedEntity.getEyeHeight() - 1.100000023841858D - var2.posY;
	                double var7 = targetedEntity.posZ - this.posZ;
	                float var10 = MathHelper.sqrt_double(var3 * var3 + var7 * var7) * 0.2F;
	                var2.setThrowableHeading(var3, var5 + (double)var10, var7, 1.6F, 6.0F);
	                this.func_85030_a("random.bow", 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
	                this.worldObj.spawnEntityInWorld(var2);                   
	            }
	            else if (this.attackCounter == (100+Math.random())*200)
	            {
                    this.attackCounter = 0;
	            }
	            if(this.attackCounter > 310)
	            {
	            	this.attackCounter = 0;
	            }
	        }
	        else if (this.attackCounter > 0)
	        {
	            --this.attackCounter;
	        }           
        }	
        int var1 = MathHelper.floor_double(this.posX);
        int var2 = MathHelper.floor_double(this.posZ);       
    	for (var1 = 0; var1 < 4; ++var1)
        {
            var2 = MathHelper.floor_double(this.posX + (double)((float)(var1 % 2 * 2 - 1) * 0.25F));
            int var3 = MathHelper.floor_double(this.posY);
            int var4 = MathHelper.floor_double(this.posZ + (double)((float)(var1 / 2 % 2 * 2 - 1) * 0.25F));
        }
    	if (!this.onGround && this.motionY < 0.0D)
        {
            this.motionY *= 0.8D;
        }
    	
    }
    
    protected int getDropItemId()
    {
        return Item.appleGold.shiftedIndex; 
    }
}

 