package cartasiane.mobs;

import net.minecraft.src.Entity;
import net.minecraft.src.MathHelper;
import net.minecraft.src.ModelBase;
import net.minecraft.src.ModelRenderer;

public class ModelDemon extends ModelBase
{
    ModelRenderer Body;
    ModelRenderer BrasG;
    ModelRenderer BrasD;
    ModelRenderer WignsD;
    ModelRenderer WignsG;
    ModelRenderer BoxTete;
    ModelRenderer BoxJG;
    ModelRenderer BoxJD;
    ModelRenderer BoxQueue;
  
    
   
    private float living;


    public ModelDemon()
    {
   	    this.textureWidth = 64;
        this.textureHeight = 32;
        this.setTextureOffset("BoxTete.Head", 25, 7);
        this.setTextureOffset("BoxTete.CorneD", 0, 11);
        this.setTextureOffset("BoxTete.CorneG", 0, 11);
        this.setTextureOffset("BoxJG.JambeG", 49, 15);
        this.setTextureOffset("BoxJG.PiedG", 0, 24);
        this.setTextureOffset("BoxJD.JambeD", 49, 15);
        this.setTextureOffset("BoxJD.PiedD", 0, 17);
        this.setTextureOffset("BoxQueue.Queue1", 49, 10);
        this.setTextureOffset("BoxQueue.Queue2", 49, 6);
        this.Body = new ModelRenderer(this, 0, 0);
        this.Body.addBox(-2.0F, 0.0F, 0.0F, 4, 6, 4);
        this.Body.setRotationPoint(0.0F, 14.0F, -3.5F);
        this.Body.setTextureSize(64, 32);
        this.Body.mirror = true;
        this.setRotation(this.Body, 0.4363323F, 0.0F, 0.0F);
        this.BrasG = new ModelRenderer(this, 5, 11);
        this.BrasG.addBox(0.0F, 0.0F, -1.0F, 2, 4, 2);
        this.BrasG.setRotationPoint(2.0F, 14.0F, -0.5F);
        this.BrasG.setTextureSize(64, 32);
        this.BrasG.mirror = true;
        this.setRotation(this.BrasG, -0.1745329F, 0.1396263F, -0.3316126F);
        this.BrasD = new ModelRenderer(this, 5, 11);
        this.BrasD.addBox(-2.0F, 0.0F, -1.0F, 2, 4, 2);
        this.BrasD.setRotationPoint(-2.0F, 14.0F, -0.5F);
        this.BrasD.setTextureSize(64, 32);
        this.BrasD.mirror = true;
        this.setRotation(this.BrasD, -0.1745329F, -0.1396263F, 0.3316126F);
        this.WignsD = new ModelRenderer(this, 26, 20);
        this.WignsD.addBox(0.0F, -6.0F, 0.0F, 0, 6, 4);
        this.WignsD.setRotationPoint(0.0F, 15.0F, 1.0F);
        this.WignsD.setTextureSize(64, 32);
        this.WignsD.mirror = true;
        this.setRotation(this.WignsD, -0.296706F, 0.5759587F, 0.0349066F);
        this.WignsG = new ModelRenderer(this, 26, 20);
        this.WignsG.addBox(0.0F, -6.0F, 0.0F, 0, 6, 4);
        this.WignsG.setRotationPoint(0.0F, 15.0F, 1.0F);
        this.WignsG.setTextureSize(64, 32);
        this.WignsG.mirror = true;
        this.setRotation(this.WignsG, -0.296706F, -0.5759587F, -0.0349066F);
        this.BoxTete = new ModelRenderer(this, "BoxTete");
        this.BoxTete.setRotationPoint(0.0F, 14.0F, -2.0F);
        this.setRotation(this.BoxTete, 0.0F, 0.0F, 0.0F);
        this.BoxTete.mirror = true;
        this.BoxTete.addBox("Head", -3.0F, -6.0F, -3.0F, 6, 6, 6);
        this.BoxTete.addBox("CorneD", -4.0F, -7.0F, 0.0F, 1, 2, 1);
        this.BoxTete.addBox("CorneG", 3.0F, -7.0F, 0.0F, 1, 2, 1);
        this.BoxJG = new ModelRenderer(this, "BoxJG");
        this.BoxJG.setRotationPoint(2.0F, 18.0F, 0.0F);
        this.setRotation(this.BoxJG, 0.0F, 0.0F, 0.0F);
        this.BoxJG.mirror = true;
        this.BoxJG.addBox("JambeG", -1.0F, 0.0F, 0.0F, 1, 4, 2);
        this.BoxJG.addBox("PiedG", -1.0F, 4.0F, -2.0F, 2, 2, 4);
        this.BoxJD = new ModelRenderer(this, "BoxJD");
        this.BoxJD.setRotationPoint(-2.0F, 18.0F, 0.0F);
        this.setRotation(this.BoxJD, 0.0F, 0.0F, 0.0F);
        this.BoxJD.mirror = true;
        this.BoxJD.addBox("JambeD", 0.0F, 0.0F, 0.0F, 1, 4, 2);
        this.BoxJD.addBox("PiedD", -1.0F, 4.0F, -2.0F, 2, 2, 4);
        this.BoxQueue = new ModelRenderer(this, "BoxQueue");
        this.BoxQueue.setRotationPoint(0.0F, 17.0F, 2.0F);
        this.setRotation(this.BoxQueue, 0.0F, 0.0F, 0.0F);
        this.BoxQueue.mirror = true;
        this.BoxQueue.addBox("Queue1", -1.0F, 0.0F, 0.0F, 2, 1, 4);
        this.BoxQueue.addBox("Queue2", -1.0F, 0.0F, 4.0F, 2, 1, 3);

      
    	}
      
    

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7)
    {
        super.render(var1, var2, var3, var4, var5, var6, var7);
        this.living = (float)Math.sin((double)(var4 / 2.0F)) * 0.2F;
        this.living *= this.living;
        this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
        this.Body.render(var7);
        this.BrasG.render(var7);
        this.BrasD.render(var7);
        this.WignsD.render(var7);
        this.WignsG.render(var7);
        this.BoxTete.render(var7);
        this.BoxJG.render(var7);
        this.BoxJD.render(var7);
        this.BoxQueue.render(var7);
    }
      

    private void setRotation(ModelRenderer var1, float var2, float var3, float var4)
    {
        var1.rotateAngleX = var2;
        var1.rotateAngleY = var3;
        var1.rotateAngleZ = var4;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7)
    {
    
        EntityDemon var8 = (EntityDemon)var7;
        this.BoxTete.rotateAngleY = var4 / (180F / (float)Math.PI);
        this.BoxTete.rotateAngleX = var5 / (180F / (float)Math.PI);
        this.BoxQueue.rotateAngleX = 1.7278761F + ((float)Math.PI / 10F) * MathHelper.cos(var1) * var2;

        if (var8.onGround)
        {
            this.WignsD.rotateAngleY = -0.5F - this.living;
            this.WignsG.rotateAngleY = 0.5F + this.living;
            this.WignsD.rotateAngleZ = -0.5F - this.living;
            this.WignsG.rotateAngleZ = 0.5F + this.living;
            this.BoxJD.rotateAngleX = MathHelper.cos(var1 * 0.6662F) * 1.4F * var2;
            this.BoxJG.rotateAngleX = MathHelper.cos(var1 * 0.6662F + (float)Math.PI) * 1.4F * var2;
        }
        else
        {
            this.WignsD.rotateAngleY = -0.5F - this.living * 10.0F;
            this.WignsG.rotateAngleY = 0.5F + this.living * 10.0F;
            this.WignsD.rotateAngleZ = -0.5F - this.living * 10.0F;
            this.WignsG.rotateAngleZ = 0.5F + this.living * 10.0F;
            this.BoxJD.rotateAngleX = 0.7F;
            this.BoxJG.rotateAngleX = 0.7F;
        }
    }
}
