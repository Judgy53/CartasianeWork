package cartasiane.fight.anim;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Map;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.action.action.Action;
import cartasiane.fight.anim.action.action.AnimSwitchAction;
import cartasiane.fight.anim.action.action.AttackAction;
import cartasiane.fight.anim.action.action.FlagAction;
import cartasiane.fight.anim.action.action.ParryAction;
import cartasiane.fight.anim.action.condition.Condition;
import cartasiane.fight.anim.action.condition.EmptyCondition;
import cartasiane.fight.anim.action.condition.FlagCondition;
import cartasiane.fight.anim.action.condition.KeyCondition;
import cartasiane.fight.anim.action.condition.MouseCondition;
import cartasiane.fight.anim.edit.ActionOptionManager;
import cartasiane.fight.anim.edit.CurveEditorPanel;

public abstract class AnimAction
{
	protected int conditionID, actionID;
	protected String name;
	protected Condition condition;
	protected Action action;
	
	public AnimAction(String name)
	{
		this.name = name;
	}
	
	public AnimAction()
	{
		
	}
	
	public AnimAction(NBTTagCompound tag)
	{
		name = tag.getString("Name");
		NBTTagCompound optionTag = tag.getCompoundTag("options");
		conditionID = optionTag.getInteger("condition");
		actionID = optionTag.getInteger("action");
		Class actions[] = {AttackAction.class, ParryAction.class, AnimSwitchAction.class, FlagAction.class};
		Class conditions[] = {EmptyCondition.class, FlagCondition.class, MouseCondition.class, KeyCondition.class};
		try {
			action = (Action)actions[actionID].getConstructor(AnimAction.class).newInstance(this);
			condition = (Condition)conditions[conditionID].getConstructor(AnimAction.class).newInstance(this);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public abstract void draw(CurveEditorPanel panel, Graphics2D g, Color col);
	public abstract void onClick(int button, float x, float y);
	public abstract void onMouseReleased(int button, float x, float y);
	public abstract void onMouseMove(int button, float x, float y);
	public int getCondition()
	{
		return conditionID;
	}
	
	public int getActionID()
	{
		return actionID;
	}
	
	public void setAction(int action)
	{
		actionID = action;
	}
	
	public void setCondition(int condition)
	{
		conditionID = condition;
	}
	
	public NBTTagCompound toNBT(Map<String, ActionOptionManager> actionOptions)
	{
		NBTTagCompound tag = saveToNBT();
		tag.setString("Name", name);
		tag.setCompoundTag("options", actionOptions.get(name).saveToNBT());
		return tag;
	}
	
	public String getName()
	{
		return name;
	}
	
	public abstract NBTTagCompound saveToNBT();

	public void setName(String text)
	{
		this.name = text;
	}
	
	public abstract boolean isInside(int t, int length);
}
