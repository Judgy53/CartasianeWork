package cartasiane.fight.anim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.src.CompressedStreamTools;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;

public class AnimLoader
{
	private static File animDirectory;
	Map<Integer, AnimFactory> anims = new HashMap<Integer, AnimFactory>();
	Map<String, Integer> names = new HashMap<String, Integer>();
	static
	{
		animDirectory = new File(Minecraft.getMinecraftDir(), "/anims/");
	}
	
	public AnimLoader()
	{
		File files[] = animDirectory.listFiles();
		if(files != null)
		for(File file : files)
		{
			if(file.getName().endsWith(".dat"))
			{
		 		loadAnim(file);
			}
		}
	}
	
	public void saveAnim(String name, NBTTagCompound tag)
	{
		anims.put(tag.getInteger("id"), new AnimFactory(tag));
		names.put(name, tag.getInteger("id"));
		File file = new File(animDirectory, name+".dat");
		NBTTagCompound baseTag = new NBTTagCompound();
		baseTag.setTag("main", tag);
        try 
        {
        	animDirectory.mkdirs();
			CompressedStreamTools.writeCompressed(baseTag, new FileOutputStream(file));
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public NBTTagCompound loadAnim(String name)
	{
		File file = new File(animDirectory, name+".dat");
        try
        {
			return (NBTTagCompound)CompressedStreamTools.readCompressed(new FileInputStream(file)).getTag("main");
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
        return null;
	}
	
	public AnimFactory loadAnim(File file)
	{
        try 
        {
        	NBTTagCompound list = (NBTTagCompound)CompressedStreamTools.readCompressed(new FileInputStream(file)).getTag("main");
        	if(list != null)
        	{
        		AnimFactory anim = new AnimFactory(list);
        		anims.put(anim.getId(), anim);
        		System.out.println(file.getName());
        		if(anim != null && file.getName().endsWith(".dat"))
        			names.put(file.getName().substring(0, file.getName().length() - 4), anim.getId());
        	}
        	else return null;
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
        return null;
	}
	
	public AnimFactory getAnim(String name)
	{
		return this.anims.get(names.get(name));
	}

	public AnimFactory getAnim(int animId)
	{
		return anims.get(animId);
	}

}
