package cartasiane.fight.anim.action.condition;

import javax.swing.JPanel;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.action.Action;
import cartasiane.net.NetCartaHandler;

public class EmptyCondition implements Action, Condition
{
	public EmptyCondition(AnimAction action)
	{
		
	}

	public void loadFromNBT(NBTTagCompound tag)
	{
		
	}

	public NBTTagCompound saveToNBT()
	{
		return new NBTTagCompound();
	}

	public void initPanel()
	{
		
	}

	public JPanel getPanel()
	{
		return new JPanel();
	}
	
	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net)
	{
		
	}
	
	public boolean satisfied(Minecraft mc, EntityPlayer player)
	{
		return true;
	}

}
