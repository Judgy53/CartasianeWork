package cartasiane.fight.anim.action.condition;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.lwjgl.input.Mouse;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.action.Action;
import cartasiane.net.NetCartaHandler;

public class MouseCondition implements Action, ActionListener, Condition
{
	public JComboBox buttonCombo, stateCombo;
	private JPanel panel;
	private AnimAction action;
	
	private int state, button;
	
	public MouseCondition(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		state = tag.getInteger("state");
		button = tag.getInteger("button");
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("state", state);
		tag.setInteger("button", button);
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		String ids[] = {"Gauche", "Milieu", "Droite"};
		buttonCombo = new JComboBox(ids);
		String actions[] = {"Up", "Down", "Clic"};
		stateCombo = new JComboBox(actions);
		buttonCombo.addActionListener(this);
		stateCombo.addActionListener(this);
		buttonCombo.setSelectedIndex(button);
		stateCombo.setSelectedIndex(state);
		panel.add(new JTextArea("Bouton "));
		panel.add(buttonCombo);
		panel.add(stateCombo);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == buttonCombo)
			button = buttonCombo.getSelectedIndex();
		else
			state = stateCombo.getSelectedIndex();
	}

	public boolean satisfied(Minecraft mc, EntityPlayer player)
	{
		return (state == 0 && Mouse.isButtonDown(button))
				|| (state == 1 && !Mouse.isButtonDown(button))
				|| (state == 2 && Mouse.isButtonDown(button));
	}

	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net)
	{
		
	}

}
