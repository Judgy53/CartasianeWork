package cartasiane.fight.anim.action.condition;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.action.Action;
import cartasiane.net.NetCartaHandler;

public class FlagCondition implements Action, ActionListener, Condition
{
	public JComboBox combo, actionCombo;
	JPanel panel;
	AnimAction action;
	private int actionID, flagID;
	
	
	public FlagCondition(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		actionID = tag.getInteger("action");
		flagID = tag.getInteger("flag");
		if(combo != null)
			combo.setSelectedIndex(actionID);
		if(actionCombo != null)
			actionCombo.setSelectedIndex(flagID);
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("flag", flagID);
		tag.setInteger("action", actionID);
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		String ids[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		combo = new JComboBox(ids);
		String actions[] = {"Activé", "Désactivé"};
		actionCombo = new JComboBox(actions);
		combo.setSelectedIndex(flagID);
		actionCombo.setSelectedIndex(actionID);
		combo.addActionListener(this);
		actionCombo.addActionListener(this);
		panel.add(new JTextArea("flag n°"));
		panel.add(combo);
		panel.add(actionCombo);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void stateChanged(ChangeEvent arg0)
	{
		
		
	}

	public void actionPerformed(ActionEvent arg0)
	{
		if(arg0.getSource() == combo)
			flagID = combo.getSelectedIndex();
		else
			actionID = actionCombo.getSelectedIndex();
	}

	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net)
	{
		
	}

	public boolean satisfied(Minecraft mc, EntityPlayer player)
	{
		
		return player.currentAnim.flags[flagID] == (actionID == 1);
	}

}
