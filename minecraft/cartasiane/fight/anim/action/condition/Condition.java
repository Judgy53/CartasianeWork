package cartasiane.fight.anim.action.condition;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;

public interface Condition
{
	public boolean satisfied(Minecraft mc, EntityPlayer player);
}
