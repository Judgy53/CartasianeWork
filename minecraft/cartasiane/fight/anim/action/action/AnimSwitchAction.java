package cartasiane.fight.anim.action.action;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.net.NetCartaHandler;
import cartasiane.net.Packet157StartAnim;

public class AnimSwitchAction implements Action, CaretListener
{
	private JTextArea nameArea;
	private JPanel panel;
	private AnimAction action;
	String name;
	
	public AnimSwitchAction(AnimAction action)
	{
		this.action = action;
		name = "test";
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		name = tag.getString("name");
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setString("name", nameArea.getText());
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		nameArea = new JTextArea(name);
		nameArea.setPreferredSize(new Dimension(60, 16));
		panel.add(new JLabel("Nom de l'animation : "));
		panel.add(nameArea);
		nameArea.addCaretListener(this);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void caretUpdate(CaretEvent arg0)
	{
		name = nameArea.getText();
	}
	
	public String getAnimName()
	{
		return name;
	}
	
	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net)
	{
		player.setAnimation(mc.animLoader.getAnim(name), 30, false);
		Minecraft.getMinecraft().getSendQueue().addToSendQueue(new Packet157StartAnim(player, 0, 30, 0, false));
	}
}
