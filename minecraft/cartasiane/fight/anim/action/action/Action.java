package cartasiane.fight.anim.action.action;

import javax.swing.JPanel;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.net.NetCartaHandler;

public interface Action
{
	public void loadFromNBT(NBTTagCompound tag);
	public NBTTagCompound saveToNBT();

	
	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net);
	
	public void initPanel();
	public JPanel getPanel();
}
