package cartasiane.fight.anim.action.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.net.NetCartaHandler;

public class FlagAction implements Action, ActionListener
{
	public JComboBox combo, actionCombo;
	private AnimAction action;
	private JPanel panel;
	private int flag, type;
	
	public FlagAction(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		flag = tag.getInteger("flag");
		type = tag.getInteger("action");
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("flag", flag);
		tag.setInteger("action", type);
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		String ids[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		combo = new JComboBox(ids);
		combo.addActionListener(this);
		String actions[] = {"Activer", "Désactiver", "Switcher"};
		actionCombo = new JComboBox(actions);
		actionCombo.addActionListener(this);
		if(action.getActionID() == 3)
		{
			combo.setSelectedIndex(flag);
			actionCombo.setSelectedIndex(type);
		}
		panel.add(actionCombo);
		panel.add(new JTextArea(" le flag n°"));
		panel.add(combo);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void actionPerformed(ActionEvent arg0)
	{
		if(arg0.getSource() == combo)
			flag = combo.getSelectedIndex();
		else
			type = actionCombo.getSelectedIndex();
	}
	
	public boolean getNewFlag(boolean var)
	{
		switch(type)
		{
		case 0 : return true;
		case 1 : return false;
		case 2 : return !var;
		default : return false;
		}
	}

	public void apply(Minecraft mc, EntityPlayer player, NetCartaHandler net)
	{
		player.currentAnim.flags[flag] = getNewFlag(player.currentAnim.flags[flag]);
		System.out.println(player.currentAnim.flags[flag]);
	}
	
}
