package cartasiane.fight.anim.action.type;

import java.awt.Color;
import java.awt.Graphics2D;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.edit.CurveEditorPanel;

public class ZoneAction extends AnimAction
{
	float x1, x2;

	public ZoneAction()
	{

	}

	public ZoneAction(String name, int condition, int action)
	{
		super(name);
		conditionID = condition;
		actionID = action;
	}

	public ZoneAction(NBTTagCompound tag)
	{
		super(tag);
		System.out.println(x1 + ", " + x2);
		x1 = tag.getFloat("x1");
		x2 = tag.getFloat("x2");
	}

	public void draw(CurveEditorPanel panel, Graphics2D g, Color col)
	{
		panel.drawZone(g, x1, x2, new Color(col.getRed(), col.getGreen(), col.getBlue(), 100));
	}

	public void onClick(int button, float x, float y)
	{
		x1 = x;
		x2 = x;
	}

	public void onMouseReleased(int button, float x, float y)
	{

	}

	public void onMouseMove(int button, float x, float y)
	{
		x2 = x;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("type", 1);
		tag.setFloat("x1", x1);
		tag.setFloat("x2", x2);
		return tag;
	}
	
	public boolean isInside(int t, int length)
	{
		if(x1 == x2)
			return false;
		return ((float)t/length >= x1) && ((float)t/length <= x2);
	}
}