package cartasiane.fight.anim.action.type;

import java.awt.Color;
import java.awt.Graphics2D;

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.edit.CurveEditorPanel;

public class PonctualAction extends AnimAction
{
	float pos = -10;

	public PonctualAction(String name)
	{
		super(name);
	}

	public PonctualAction(String name, int condition, int action)
	{
		super(name);
		conditionID = condition;
		actionID = action;
	}

	public void draw(CurveEditorPanel panel, Graphics2D g, Color col)
	{
		panel.drawVerticalLine(g, pos, col);
	}

	public PonctualAction(NBTTagCompound tag)
	{
		super(tag);
		pos = tag.getFloat("pos");
	}


	public void onClick(int button, float x, float y)
	{
		pos = x;
	}

	public void onMouseReleased(int button, float x, float y)
	{

	}

	public void onMouseMove(int button, float x, float y)
	{
		pos = x;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("type", 0);
		tag.setFloat("pos", pos);
		return tag;
	}

	public boolean isInside(int t, int length)
	{
		System.out.println(pos);
		return ((float)t/length <= pos) && ((float)(t+1)/length > pos);
	}
}