package cartasiane.fight.anim;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.Packet7UseEntity;
import cartasiane.fight.anim.action.action.AnimSwitchAction;
import cartasiane.net.NetCartaHandler;
import cartasiane.net.Packet157StartAnim;
import cartasiane.util.BezierCurve;

public class Anim
{
    public boolean flags[] = new boolean[10];
	private boolean left;
	private int length, time, speed;
	protected EntityPlayer player;
	
	private BezierCurve[] curves;
	private AnimAction[] actions;
	private boolean init = false;

	public Anim(EntityPlayer player, int length, boolean left, BezierCurve[] curves, AnimAction[] actions)
	{
		for(int i=0; i<10; i++)
			flags[i] = false;
		this.player = player;
		this.length = length;
		this.time = 0;
		this.speed = 1;
		this.left = left;
		this.curves = curves;
		this.actions = actions;
	}
	
	protected EntityPlayer getPlayer()
	{
		return player;
	}
	
	public Anim restart()
	{
		time = 0;
		return this;
	}
	
	public void anim()
	{
		time += speed;
		onUpdate();
		if(time >= length)
			player.setAnimation(null, 0, false);
	}
	
	public float ratio()
	{
		return time/(float)length;
	}
	
	protected boolean isAtFrame(float ratio)
	{
		return ratio() < ratio && (time+1)/(float)length >= ratio;
	}
	
	public boolean isLeft()
	{
		return left;
	}
	
	public int getLength()
	{
		return length;
	}

	protected void onAnimFinished()
	{
		
	}
	
	public void onClick(int button)
	{
		
	}
	
	public void onJump()
	{
		
	}
	
	public void onMove()
	{
		
	}
	
	public void onUpdate()
	{
		for(AnimAction action : actions)
		{
			System.out.println(action.name);
			if(action.isInside(time, length) && action.condition != null && action.condition.satisfied(Minecraft.getMinecraft(), player))
			{
				NetCartaHandler net = null;
				if(player == Minecraft.getMinecraft().thePlayer)
					net = Minecraft.getMinecraft().getSendQueue();
				action.action.apply(Minecraft.getMinecraft(), player, net);
			}
		}
	}

	public float calc(int curve)
	{
		return curves[curve].getFunction().calc(ratio());
	}
}
