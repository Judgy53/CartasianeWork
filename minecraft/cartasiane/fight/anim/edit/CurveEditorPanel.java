package cartasiane.fight.anim.edit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import cartasiane.fight.anim.AnimAction;
import cartasiane.util.AnimPoint;
import cartasiane.util.BezierCurve;

public class CurveEditorPanel extends JPanel implements MouseListener, MouseMotionListener, KeyListener, ActionListener, ChangeListener
{
	float xOrigin = 0.0f, yOrigin = 0.5f, unitDimX = 15, unitDimY = 15, zoomX = 20, zoomY = 20;
	float playPos = 0, playSpeed = 0.01f;
	public boolean playing;
	Color[] colors = {Color.blue, Color.blue, Color.blue, Color.green, Color.green, Color.green, Color.magenta, Color.magenta, Color.magenta, Color.blue, Color.blue, Color.blue, Color.green, Color.green, Color.green, Color.magenta, Color.magenta, Color.magenta, Color.blue, Color.blue, Color.blue, Color.green, Color.green, Color.green, Color.magenta, Color.magenta, Color.magenta, Color.blue, Color.blue, Color.blue, Color.green, Color.green, Color.green, Color.magenta, Color.magenta, Color.magenta};
	BezierCurve curves[];
	
	boolean actionSelected = false;
	
	ActionList listModel;
	JList list;
	
	JCheckBox boxes[];
	
	int dragX, dragY;
	float lastUnitDimX, lastUnitDimY;
	
	int curveSelected = -1, pointSelected = -1;
	int mouseOverCurve = -1, mouseOverPoint = -1;
	
	boolean mousePressed = false;
	private JSpinner frameSpinner;
	private boolean leftDrag;
	CurveEditorWindow window;
	
	public CurveEditorPanel(JCheckBox[] boxes, JSpinner frameSpinner, JList list, ActionList listModel, CurveEditorWindow window)
	{
		this.list = list;
		this.listModel = listModel;
		this.window = window;
		curves = new BezierCurve[36];
		for(int i=0; i<curves.length; i++)
			curves[i] = new BezierCurve();
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.boxes = boxes;
		for(JCheckBox box : boxes)
			box.addActionListener(this);
		this.frameSpinner = frameSpinner;
		frameSpinner.addChangeListener(this);
	}
	
	public void paintComponent(Graphics graph)
	{
	      int w = this.getSize().width, h = this.getSize().height;
	      super.paintComponent(graph);

	      Graphics2D g = (Graphics2D) graph;
	      
	      g.setColor(Color.gray);
	      for(float x = w*xOrigin-unitDimX; x > 0; x-=unitDimX)
	    	  g.drawLine((int)x, 0, (int)x, h);
	      for(float x = w*xOrigin+unitDimX; x < w; x+=unitDimX)
	    	  g.drawLine((int)x, 0, (int)x, h);
	      for(float y = h*yOrigin-unitDimY; y > 0; y-=unitDimY)
	    	  g.drawLine(0, (int)y, w, (int)y);
	      for(float y = h*yOrigin+unitDimY; y < h; y+=unitDimY)
	    	  g.drawLine(0, (int)y, w, (int)y);

	      for(int i=0; i<3;i++)
	    	  g.drawLine(0, (int)(h*yOrigin + i*unitDimY*zoomY), w, (int)(h*yOrigin + i*unitDimY*zoomY));
	     
	      g.setColor(Color.black);

	      g.drawLine(0, (int)(h*yOrigin), w, (int)(h*yOrigin));
	      g.drawLine((int)(w*xOrigin), 0, (int)(w*xOrigin), h);
	      g.drawRect(0, 0, w-1, h-1);
	      curves[0].getPoints();

	      g.setColor(Color.red);
	      g.drawLine((int)(w*xOrigin + unitDimX*zoomX), 0, (int)(w*xOrigin + unitDimX*zoomX), h);

	      for(float y = h*yOrigin-unitDimY*zoomY/4; y > 0; y-=unitDimY*zoomY/4)
	    	  g.drawLine(0, (int)y, (int)w, (int)y);
	      for(float y = h*yOrigin+unitDimY*zoomY/4; y < h; y+=unitDimY*zoomY/4)
	    	  g.drawLine(0, (int)y, (int)w, (int)y);
	      
	      
	      for(int i=0; i<curves.length; i++)
	      {
	    	  if(boxes[i].isSelected())
	    	  {
	    		  Vector<AnimPoint> points = curves[i].getPoints();
	    		  int[] x = new int[points.size()], y = new int[points.size()];
	    		  g.setColor(colors[i]);
	    		  int thickness = 1;
	    		  if(curveSelected == i)
	    			  thickness = 2;
	    		  BasicStroke bs1 = new BasicStroke(thickness, BasicStroke.CAP_BUTT,
	    	                BasicStroke.JOIN_ROUND, 1.0f, null, 2f );
	    		  g.setStroke(bs1);
	    		  for(int j=0;j<points.size(); j++)
	    		  {
	    			  g.drawRect((int)(w*xOrigin + points.get(j).time*unitDimX*zoomX)-1, (int)(h*yOrigin + points.get(j).var*unitDimY*zoomY)-1, 3, 3);
	    			  x[j] = (int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
	    			  y[j] = (int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
	    		  }
	    		  g.drawPolyline(x, y, points.size());
	    		  if(mouseOverCurve == i)
	    		  {
		    		  g.setColor(Color.red);
	    			  g.drawRect((int)(w*xOrigin + points.get(mouseOverPoint).time*unitDimX*zoomX)-1, (int)(h*yOrigin + points.get(mouseOverPoint).var*unitDimY*zoomY)-1, 3, 3);
	    		  }
	    	  }
	      }
	      
	      for(int i=0; i<listModel.getSize(); i++)
	      {
	    	  Color col;
	    	  if(i == list.getSelectedIndex() && actionSelected)
	    		  col = Color.green;
	    	  else col = Color.red;
	    	  if(listModel.getAction(i) != null)
	    		  ((AnimAction)listModel.getAction(i)).draw(this, g, new Color(col.getRed(), col.getGreen(), col.getBlue(), 100));
	      }
	      g.setColor(Color.yellow);
	      g.drawLine((int)(w*xOrigin + playPos*unitDimX*zoomX), 0, (int)(w*xOrigin + playPos*unitDimX*zoomX), h);
	      
	}
	
	public NBTTagList saveToNBT()
	{
		NBTTagList list = new NBTTagList();
		
		for(int i=0; i<curves.length; i++)
		{
			list.appendTag(curves[i].saveToNBT());
		}
		return list;
	}
	
	public void loadAnims(NBTTagCompound tag)
	{
		NBTTagList list = tag.getTagList("curves");
		if(list == null)
		{
			for(int i=0; i<36; i++)
				curves[i] = new BezierCurve();
		}
		else
		{
			curves = new BezierCurve[list.tagCount()];
			for(int i=0; i<list.tagCount(); i++)
				curves[i] = new BezierCurve((NBTTagList)list.tagAt(i));
		}
		NBTTagList actionList = tag.getTagList("actions");
		for(int i=0; i<actionList.tagCount(); i++)
		{
			window.addAction((NBTTagCompound)actionList.tagAt(i));
		}
	}

	public void mouseClicked(MouseEvent arg0)
	{
	      int w = this.getSize().width, h = this.getSize().height;
	      if(arg0.getButton() == arg0.BUTTON2)
	      {
	    	  	for(int i=0; i<curves.length; i++)
				{
	    	  		if(boxes[i].isSelected())
	    	  		{
	    	  			Vector<AnimPoint> points = curves[i].getPoints();
	    	  			for(int j=0; j<curves[i].countPoints(); j++)
	    	  			{
	    	  				float dx = arg0.getX()-(int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
	    	  				float dy = arg0.getY()-(int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
	    	  				if(dx*dx < 16 && dy*dy < 16)
	    	  				{
	    	  					curves[i].delPoint(points.get(j).time, points.get(j).var);
	    	  					curveSelected = i;
	    	  				}
	    	  			}
	    	  		}
				}
	      }
	      else
	      {
				for(int i=0; i<curves.length; i++)
				{
					if(boxes[i].isSelected())
	    	  		{
						Vector<AnimPoint> points = curves[i].getPoints();
						for(int j=0; j<curves[i].countPoints(); j++)
						{
							float dx = arg0.getX()-(int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
							float dy = arg0.getY()-(int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
							if(dx*dx < 8*8 && dy*dy < 8*8)
							{
								this.curveSelected = i;
							}
						}
	    	  		}
				}
	      }
	      repaint();
	}

	public void mouseEntered(MouseEvent arg0)
	{
		
	}

	public void mouseExited(MouseEvent arg0)
	{
		
	}

	public void mousePressed(MouseEvent arg0)
	{
		int w = this.getSize().width, h = this.getSize().height;
		if(actionSelected && arg0.getButton() == arg0.BUTTON1)
		{
			float x = ((float)arg0.getX()-w*xOrigin)/(unitDimX*zoomX);
			float y = ((float)arg0.getY()-h*yOrigin)/(unitDimY*zoomY);
			if(list.getSelectedIndex() >= 0 && listModel.getAction(list.getSelectedIndex()) != null)
				((AnimAction) listModel.getAction(list.getSelectedIndex())).onClick(arg0.getButton(), x, y);
		}
		if(arg0.getButton() == arg0.BUTTON2)
		{
			lastUnitDimX = unitDimX;
			lastUnitDimY = unitDimY;
		}
		if(arg0.getButton() == arg0.BUTTON1)
		{
			this.leftDrag = true;
		}
		if(!actionSelected && arg0.getButton() == arg0.BUTTON1)
		{
			boolean selectedPoint = false;
			for(int i=0; i<curves.length; i++)
			{
				if(boxes[i].isSelected())
				{
					Vector<AnimPoint> points = curves[i].getPoints();
					for(int j=0; j<curves[i].countPoints(); j++)
					{
						float dx = arg0.getX()-(int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
						float dy = arg0.getY()-(int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
						if(dx*dx < 8*8 && dy*dy < 8*8)
						{
							this.curveSelected = i;
							this.pointSelected = j;
							selectedPoint = true;
						}
					}
				}
			}
			if(!selectedPoint && curveSelected >= 0)
			{
				float x = ((float)arg0.getX()-w*xOrigin)/(unitDimX*zoomX);
				float y = ((float)arg0.getY()-h*yOrigin)/(unitDimY*zoomY);
				curves[curveSelected].addPoint(x, y);
				for(int i=0; i<curves.length; i++)
				{
					if(boxes[i].isSelected())
					{
						Vector<AnimPoint> points = curves[i].getPoints();
						for(int j=0; j<curves[i].countPoints(); j++)
						{
							float dx = arg0.getX()-(int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
							float dy = arg0.getY()-(int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
							if(dx*dx < 8*8 && dy*dy < 8*8)
							{
								this.curveSelected = i;
								this.pointSelected = j;
								selectedPoint = true;
							}
						}
					}
				}
			}
		}
		dragX = arg0.getX();
		dragY = arg0.getY();
	}

	public void mouseReleased(MouseEvent arg0)
	{
		this.pointSelected = -1;
		if(arg0.getButton() == arg0.BUTTON1)
			this.leftDrag = false;
	}

	public void mouseDragged(MouseEvent arg0)
	{
		int w = this.getSize().width, h = this.getSize().height;
		if(actionSelected && leftDrag)
		{
			float x = ((float)arg0.getX()-w*xOrigin)/(unitDimX*zoomX);
			float y = ((float)arg0.getY()-h*yOrigin)/(unitDimY*zoomY);
			if(list.getSelectedIndex() >= 0 && listModel.getAction(list.getSelectedIndex()) != null)
				((AnimAction) listModel.getAction(list.getSelectedIndex())).onMouseMove(arg0.getButton(), x, y);
		}
		if(!actionSelected && pointSelected >= 0)
		{
			this.curves[curveSelected].getPoints().get(pointSelected).time += ((float)(arg0.getX()-dragX))/unitDimX/zoomX;
			this.curves[curveSelected].getPoints().get(pointSelected).var += ((float)(arg0.getY()-dragY))/unitDimY/zoomY;
			if(pointSelected == 0)
				this.curves[curveSelected].getPoints().get(pointSelected).time = 0;
			if(pointSelected == this.curves[curveSelected].countPoints()-1)
				this.curves[curveSelected].getPoints().get(pointSelected).time = 1;
			dragX = arg0.getX();
			dragY = arg0.getY();
			pointSelected = this.curves[curveSelected].sortPoints(pointSelected);
		}
		else if(!actionSelected)
		{
			if(arg0.isShiftDown())
			{
				unitDimX = ((float)arg0.getX()/(float)dragX)*lastUnitDimX;
				unitDimY = ((float)arg0.getY()/(float)dragY)*lastUnitDimY;
			}
			else if(!leftDrag)
			{
				xOrigin += ((float)(arg0.getX()-dragX))/getSize().width;
				yOrigin += ((float)(arg0.getY()-dragY))/getSize().height;
				dragX = arg0.getX();
				dragY = arg0.getY();			
			}
		}
		repaint();
	}

	public void mouseMoved(MouseEvent arg0)
	{
		int w = this.getSize().width, h = this.getSize().height;
		mouseOverCurve = -1;
		mouseOverPoint = -1;
		for(int i=0; i<curves.length; i++)
		{
			if(boxes[i].isSelected())
			{
				Vector<AnimPoint> points = curves[i].getPoints();
				for(int j=0; j<points.size(); j++)
				{
					float dx = arg0.getX()-(int)(w*xOrigin + points.get(j).time*unitDimX*zoomX);
					float dy = arg0.getY()-(int)(h*yOrigin + points.get(j).var*unitDimY*zoomY);
					if(dx*dx < 8*8 && dy*dy < 8*8)
					{
						mouseOverCurve = i;
						mouseOverPoint = j;
					}
				}
			}
		}
		repaint();
	}

	public void keyPressed(KeyEvent arg0)
	{
		
	}

	public void keyReleased(KeyEvent arg0)
	{
		
	}

	public void keyTyped(KeyEvent arg0)
	{
		
	}

	public void actionPerformed(ActionEvent arg0)
	{
		for(int i=0; i<boxes.length; i++)
		{
			if(boxes[i] == arg0.getSource())
			{
				curveSelected = i;
				break;
			}
		}
		repaint();
	}
	
	public void anim()
	{
		if(playing)
		{
			playPos += playSpeed;
			frameSpinner.setValue(playPos/playSpeed);
			frameSpinner.setPreferredSize(new Dimension(33, 18));
			if(playPos > 1)
				playPos = 0;
			repaint();
		}
	}
	
	public void setAnimSpeed(int frames)
	{
		playSpeed = 1f/(float)frames;
	}
	
	public void rewind()
	{
		playPos = 0;
	}
	
	public float calc(int curve)
	{
		return curves[curve].getFunction().calc(playPos);
	}
	
	public void resetCurves()
	{
		for(int i=0; i<curves.length;i++)
		{
			curves[i] = new BezierCurve();
		}
		repaint();
	}

	public float getAnimRatio()
	{
		return playPos;
	}

	public void stateChanged(ChangeEvent arg0)
	{
		float value = 0;
		if(frameSpinner.getValue() instanceof Float)
			value = (Float)frameSpinner.getValue();
		if(frameSpinner.getValue() instanceof Integer)
			value = (Integer)frameSpinner.getValue();
		playPos = playSpeed*value;
		repaint();
	}
	
	public void drawLine(Graphics2D g, float x1, float y1, float x2, float y2, Color col)
	{
        int w = this.getSize().width, h = this.getSize().height;
		x1 = (int)(w*xOrigin + x1*unitDimX*zoomX);
		y1 = (int)(h*yOrigin + y1*unitDimY*zoomY);
		x2 = (int)(w*xOrigin + x2*unitDimX*zoomX);
		y2 = (int)(h*yOrigin + y2*unitDimY*zoomY);
		g.setColor(col);
		g.drawLine((int)x1, (int)y1, (int)x2, (int)y2);
	}
	
	public void drawVerticalLine(Graphics2D g, float x, Color col)
	{
        int w = this.getSize().width, h = this.getSize().height;
		x = (int)(w*xOrigin + x*unitDimX*zoomX);
		g.setColor(col);
		g.drawLine((int)x, (int)0, (int)x, (int)h+1);
	}
	
	public void drawZone(Graphics2D g, float x1, float x2, Color col)
	{
		int w = this.getSize().width, h = this.getSize().height;
		x1 = (int)(w*xOrigin + x1*unitDimX*zoomX);
		x2 = (int)(w*xOrigin + x2*unitDimX*zoomX);
		g.setColor(col);
		g.fillRect((int)Math.min(x1, x2), 0, (int)(Math.max(x1, x2)-Math.min(x1, x2)), h);
	}
}
