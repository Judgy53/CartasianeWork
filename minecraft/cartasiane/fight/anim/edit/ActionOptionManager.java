package cartasiane.fight.anim.edit;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.action.Action;
import cartasiane.fight.anim.action.action.AnimSwitchAction;
import cartasiane.fight.anim.action.action.AttackAction;
import cartasiane.fight.anim.action.action.FlagAction;
import cartasiane.fight.anim.action.action.ParryAction;
import cartasiane.fight.anim.action.condition.EmptyCondition;
import cartasiane.fight.anim.action.condition.FlagCondition;
import cartasiane.fight.anim.action.condition.KeyCondition;
import cartasiane.fight.anim.action.condition.MouseCondition;

public class ActionOptionManager extends JPanel implements ActionListener
{
	public CardLayout actionLayout, conditionLayout;
	private JComboBox conditionCombo;
	private JComboBox actionCombo;
	private int actionType, conditionType;
	private Action[] optionList = new Action[4];
	private Action[] conditionList = new Action[4];
	JPanel actionPanel, conditionPanel;
	
	
	public ActionOptionManager(AnimAction action, JComboBox actionCombo, JComboBox conditionCombo, NBTTagCompound tag)
	{
		super();
		this.actionCombo = actionCombo;
		actionCombo.addActionListener(this);
		this.conditionCombo = conditionCombo;
		conditionCombo.addActionListener(this);
		actionLayout = new CardLayout();
		conditionLayout = new CardLayout();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(actionCombo);
		actionPanel = new JPanel(actionLayout);
		conditionPanel = new JPanel(conditionLayout);
		optionList[0] = new AttackAction(action);
		optionList[1] = new ParryAction(action);
		optionList[2] = new AnimSwitchAction(action);
		optionList[3] = new FlagAction(action);
		String names[] = {"Attaque", "Parade", "Changement d'animation", "Activation d'un flag"};
		if(tag != null)
		{
			optionList[tag.getInteger("action")].loadFromNBT(tag.getCompoundTag("actionOptions"));
			actionLayout.show(actionPanel, names[tag.getInteger("action")]);
		}
		for(int i=0; i<names.length; i++)
		{
			optionList[i].initPanel();
			actionPanel.add(optionList[i].getPanel(), names[i]);
		}
		String condNames[] = {"Aucune", "Flag activ�", "Clic", "Touche"};
		conditionList[0] = new EmptyCondition(action);
		conditionList[1] = new FlagCondition(action);
		conditionList[2] = new MouseCondition(action);
		conditionList[3] = new KeyCondition(action);
		if(tag != null)
		{
			conditionList[tag.getInteger("condition")].loadFromNBT(tag.getCompoundTag("conditionOptions"));
		}
		for(int i=0; i<condNames.length; i++)
		{
			conditionList[i].initPanel();
			conditionPanel.add(conditionList[i].getPanel(), condNames[i]);
		}
		if(tag != null)
			conditionLayout.show(conditionPanel, condNames[tag.getInteger("condition")]);
		add(actionPanel);
		add(conditionCombo);
		add(conditionPanel);
	}

	public void actionPerformed(ActionEvent arg0)
	{
		if(arg0.getSource() == actionCombo)
		{
			actionLayout.show(actionPanel, (String)actionCombo.getSelectedItem());
			actionType = actionCombo.getSelectedIndex();
		}
		
		if(arg0.getSource() == conditionCombo)
		{
			conditionLayout.show(conditionPanel, (String)conditionCombo.getSelectedItem());
			conditionType = conditionCombo.getSelectedIndex();
		}
	}
	
	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setCompoundTag("actionOptions", optionList[actionType].saveToNBT());
		tag.setCompoundTag("conditionOptions", conditionList[conditionType].saveToNBT());
		tag.setInteger("action", actionCombo.getSelectedIndex());
		tag.setInteger("condition", conditionCombo.getSelectedIndex());
		return tag;
	}
}
