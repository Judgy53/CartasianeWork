package cartasiane.fight.anim.edit;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.minecraft.client.Minecraft;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.type.PonctualAction;
import cartasiane.fight.anim.action.type.ZoneAction;

public class CurveEditorWindow extends JDialog implements ActionListener, ChangeListener, ListSelectionListener
{
	Minecraft mc;
	JButton saveButton, loadButton, newButton, playButton, rewindButton, copyButton, pasteButton;
	JSpinner animLength, curFrame;
	CurveEditorPanel curveEditor;
	CurveEditorManager manager = new CurveEditorManager(this);
	JTabbedPane tab;
	JTextField nameField;
	
	Float clipboard[];
	
	ActionList actionListModel;
	JList actionList;
	JComboBox actionCombo, conditionCombo;
	JButton addActionButton, deleteActionButton, actionTypeButton;
	JPanel cardPanel;
	CardLayout cardLayout = new CardLayout();
	
	public CurveEditorWindow(Minecraft mc)
	{
		this.mc = mc;
		init();
	}
	
	public void init()
	{
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		setSize(600, 400);
		setTitle("Animation Editor");
		
		JCheckBox[] boxes = new JCheckBox[36];
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
			tab = new JTabbedPane();
			JPanel thirdPersonTab = new JPanel();
			thirdPersonTab.setLayout(new BoxLayout(thirdPersonTab, BoxLayout.X_AXIS));
			JPanel firstPersonTab = new JPanel();
			firstPersonTab.setLayout(new BoxLayout(firstPersonTab, BoxLayout.X_AXIS));
			
			String[] animNames = {"rotArm", "translArm", "rotWeapon"};
			String[] animAxes = {"X", "Y", "Z"};
			JPanel TPRPanel = new JPanel();
			JPanel FPRPanel = new JPanel();
			TPRPanel.setLayout(new BoxLayout(TPRPanel, BoxLayout.Y_AXIS));
			FPRPanel.setLayout(new BoxLayout(FPRPanel, BoxLayout.Y_AXIS));
			
			for(int i=0; i<animNames.length; i++)
			{
				for(int j=0; j<animAxes.length; j++)
				{
						JPanel curvePanel = new JPanel();
						curvePanel.setLayout(new BoxLayout(curvePanel, BoxLayout.X_AXIS));
						String name = "R"+animNames[i]+animAxes[j];
						JCheckBox box = new JCheckBox(name);
						curvePanel.add(box);
						boxes[3*i+j] = box;
						curvePanel.setAlignmentX(LEFT_ALIGNMENT);
						TPRPanel.add(curvePanel);
				}
			}
			for(int i=0; i<animNames.length; i++)
			{
				for(int j=0; j<animAxes.length; j++)
				{
						JPanel curvePanel = new JPanel();
						curvePanel.setLayout(new BoxLayout(curvePanel, BoxLayout.X_AXIS));
						String name = "R"+animNames[i]+animAxes[j];
						JCheckBox box = new JCheckBox(name);
						curvePanel.add(box);
						boxes[18+3*i+j] = box;
						curvePanel.setAlignmentX(LEFT_ALIGNMENT);
						FPRPanel.add(curvePanel);
				}
			}
			thirdPersonTab.add(Box.createHorizontalGlue());
			firstPersonTab.add(Box.createHorizontalGlue());
			thirdPersonTab.add(TPRPanel);
			firstPersonTab.add(FPRPanel);
			thirdPersonTab.add(Box.createHorizontalGlue());
			firstPersonTab.add(Box.createHorizontalGlue());
			JPanel TPLPanel = new JPanel();
			JPanel FPLPanel = new JPanel();
			TPLPanel.setLayout(new BoxLayout(TPLPanel, BoxLayout.Y_AXIS));
			FPLPanel.setLayout(new BoxLayout(FPLPanel, BoxLayout.Y_AXIS));
			for(int i=0; i<animNames.length; i++)
			{
				for(int j=0; j<animAxes.length; j++)
				{
						JPanel curvePanel = new JPanel();
						curvePanel.setLayout(new BoxLayout(curvePanel, BoxLayout.X_AXIS));
						String name = "L"+animNames[i]+animAxes[j];
						JCheckBox box = new JCheckBox(name);
						curvePanel.add(box);
						boxes[9+3*i+j] = box;
						curvePanel.setAlignmentX(LEFT_ALIGNMENT);
						TPLPanel.add(curvePanel);
				}
			}

			for(int i=0; i<animNames.length; i++)
			{
				for(int j=0; j<animAxes.length; j++)
				{
						JPanel curvePanel = new JPanel();
						curvePanel.setLayout(new BoxLayout(curvePanel, BoxLayout.X_AXIS));
						String name = "L"+animNames[i]+animAxes[j];
						JCheckBox box = new JCheckBox(name);
						curvePanel.add(box);
						boxes[27+3*i+j] = box;
						curvePanel.setAlignmentX(LEFT_ALIGNMENT);
						FPLPanel.add(curvePanel);
				}
			}
			thirdPersonTab.add(TPLPanel);
			firstPersonTab.add(FPLPanel);
			tab.addTab("First Person", firstPersonTab);
			tab.addTab("Third Person", thirdPersonTab);
			tab.addTab("Combos", createComboPanel());

			JPanel editorPanel = new JPanel();
			editorPanel.setLayout(new BoxLayout(editorPanel, BoxLayout.Y_AXIS));
			
			JPanel playPanel = new JPanel();
			playPanel.setMaximumSize(new Dimension(500, 50));
			rewindButton = new JButton("Rewind");
			rewindButton.addActionListener(this);
			playPanel.add(rewindButton);
			
			playButton = new JButton("Play");
			playButton.addActionListener(this);
			playButton.setPreferredSize(new Dimension(80, 26));
			playButton.setMaximumSize(new Dimension(80, 26));
			playButton.setMinimumSize(new Dimension(80, 26));
			playPanel.add(playButton);
			
			playPanel.add(new JLabel("Dur�e : "));

			curFrame = new JSpinner();
			curFrame.addChangeListener(this);
			curFrame.setValue(0);
			playPanel.add(curFrame);
			
			playPanel.add(new JLabel("/"));
			
			animLength = new JSpinner();
			animLength.addChangeListener(this);
			animLength.setValue(20);
			playPanel.add(animLength);

			playPanel.add(new JLabel("frames"));
			
			editorPanel.add(playPanel);
			curveEditor = new CurveEditorPanel(boxes, curFrame, actionList, actionListModel, this);
			editorPanel.add(curveEditor);
			editorPanel.setBorder(BorderFactory.createBevelBorder(3));
				JPanel buttonsPanel = new JPanel();
				buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
				buttonsPanel.setMaximumSize(new Dimension(500, 50));
				copyButton = new JButton("Copy");
				pasteButton = new JButton("Paste");
				saveButton = new JButton("Save");
				loadButton = new JButton("Load");
				newButton = new JButton("New");
				copyButton.addActionListener(this);
				pasteButton.addActionListener(this);
				saveButton.addActionListener(this);
				loadButton.addActionListener(this);
				newButton.addActionListener(this);
				buttonsPanel.add(copyButton);
				buttonsPanel.add(pasteButton);
				buttonsPanel.add(Box.createHorizontalGlue());
				buttonsPanel.add(saveButton);
				buttonsPanel.add(loadButton);
				buttonsPanel.add(newButton);
			editorPanel.add(buttonsPanel);
			panel.add(editorPanel, BorderLayout.CENTER);
			
		JPanel rightPanel = new JPanel();
		tab.setPreferredSize(new Dimension(260, 100000));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.X_AXIS));
		rightPanel.add(tab);
		tab.addChangeListener(manager);
		panel.add(rightPanel, BorderLayout.LINE_END);
		
		setContentPane(panel);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == saveButton)
		{
			String name = JOptionPane.showInputDialog("Nom de l'animation");
			int id = Integer.parseInt(JOptionPane.showInputDialog("Id de l'animation"));
			mc.animLoader.saveAnim(name, saveToNBT(id));
		}
		if(event.getSource() == loadButton)
		{
			String name = JOptionPane.showInputDialog("Nom de l'animation");
			clearActions();
			curveEditor.loadAnims(mc.animLoader.loadAnim(name));
			curveEditor.repaint();
		}
		if(event.getSource() == newButton)
		{
			curveEditor.resetCurves();
			clearActions();
		}
		if(event.getSource() == playButton)
		{
			curveEditor.playing = !curveEditor.playing;
			if(curveEditor.playing)
				playButton.setText("Pause");
			else
				playButton.setText("Play");
		}
		if(event.getSource() == copyButton)
		{
			clipboard = new Float[curveEditor.boxes.length];
			for(int i=0; i<clipboard.length; i++)
			{
				if(curveEditor.boxes[i].isSelected())
				{
					clipboard[i] = curveEditor.calc(i);
				}
				else clipboard[i] = null;
			}
		}
		if(event.getSource() == pasteButton)
		{
			for(int i=0; i<clipboard.length; i++)
			{
				if(clipboard[i] != null)
				{
					curveEditor.curves[i].addPoint(this.getAnimRatio(), clipboard[i]);
				}
				else clipboard[i] = null;
			}
		}
		curveEditor.repaint();
	}
	
	private void clearActions()
	{
		actionListModel.clear();
		cardPanel.removeAll();
		repaint();
	}

	private NBTTagCompound saveToNBT(int id)
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("id", id);
		tag.setTag("curves", curveEditor.saveToNBT());
		
		NBTTagList actionlist = new NBTTagList();
		
		for(AnimAction action : actionListModel.getActionList())
		{
			actionlist.appendTag(action.toNBT(actionOptions));
		}
		tag.setTag("actions", actionlist);
		return tag;
	}

	public void anim()
	{
		curveEditor.anim();
	}
	
	public void stateChanged(ChangeEvent arg0)
	{
		if(curveEditor != null)
			curveEditor.setAnimSpeed((Integer)animLength.getValue());
	}
	
	public float calc(int curve)
	{
		return curveEditor.calc(curve);
	}

	public float getAnimRatio()
	{
		return curveEditor.getAnimRatio();
	}
	
	public JPanel createComboPanel()
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		actionListModel = new ActionList();
		actionList = new JList(actionListModel);
		actionList.setPreferredSize(new Dimension(50, 400));
		actionList.addListSelectionListener(this);
		mainPanel.add(actionList);
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.PAGE_AXIS));
		String actionTypes[] = {"Attaque", "Parade", "Changement d'animation", "Activation d'un flag"};
		
		JPanel addPanel = new JPanel();
		
		cardPanel = new JPanel(cardLayout);
		cardPanel.add(Box.createVerticalGlue(), "");
		optionPanel.add(cardPanel);
		
		nameField = new JTextField();
		nameField.addActionListener(manager);
		nameField.setPreferredSize(new Dimension(100, 20));
		addPanel.add(nameField);
		addActionButton = new JButton("Add");
		addActionButton.addActionListener(manager);
		addPanel.add(addActionButton);
		optionPanel.add(addPanel);
		deleteActionButton = new JButton("Delete");
		deleteActionButton.addActionListener(manager);
		optionPanel.add(deleteActionButton);
		optionPanel.add(Box.createVerticalGlue());
		
		mainPanel.add(optionPanel);
		
		return mainPanel;
	}
	
	public void valueChanged(ListSelectionEvent arg0)
	{
		if(getSelectedAnimAction() != null)
		{
//			actionCombo.setSelectedIndex(getSelectedAnimAction().getAction());
//			conditionCombo.setSelectedIndex(getSelectedAnimAction().getCondition());
//			nameField.setText(getSelectedAnimAction().getName());
			cardLayout.show(cardPanel, (String)this.actionList.getSelectedValue());
		}
		repaint();
	}
	
	public AnimAction getSelectedAnimAction()
	{
		if(actionList.getSelectedIndex() >= 0)
			return (AnimAction)actionListModel.getAction(actionList.getSelectedIndex());
		return null;
	}
	
	public void updateActionList()
	{
		if(getSelectedAnimAction() != null)
		{
			cardLayout.show(cardPanel, getSelectedAnimAction().getName());
		}
		repaint();
	}
	
	public JPanel createOptionPanel(String name, AnimAction action, NBTTagCompound tag)
	{
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.PAGE_AXIS));
		String actionTypes[] = {"Attaque", "Parade", "Changement d'animation", "Activation d'un flag"};
		JComboBox actionCombo = new JComboBox(actionTypes);
		actionCombo.addActionListener(manager);
		actionCombo.setSelectedIndex(action.getActionID());
		String condNames[] = {"Aucune", "Flag activ�", "Clic", "Touche"};
		conditionCombo = new JComboBox(condNames);
		conditionCombo.addActionListener(manager);
		conditionCombo.setSelectedIndex(action.getCondition());
		ActionOptionManager actionOption = new ActionOptionManager(action, actionCombo, conditionCombo, tag);
		actionOptions.put(name, actionOption);
		optionPanel.add(actionOption);
		actionTypeButton = new JButton("Zone");
		optionPanel.add(actionTypeButton);
		actionTypeButton.addActionListener(manager);
		optionPanel.add(Box.createVerticalGlue());
		return optionPanel;
	}
	
	public void addAction()
	{
		String name = nameField.getText();
		
		AnimAction action = new ZoneAction(name, 0, 0);
		actionListModel.addAction(action);
		cardPanel.add(createOptionPanel(name, action, null), name);
		cardLayout.show(cardPanel, name);
		nameField.setText("");
	}
	
	public void addAction(NBTTagCompound tag)
	{
		String name = tag.getString("Name");
		AnimAction action;
		if(tag.getInteger("type") == 0)
			action = new ZoneAction(tag);
		else action = new PonctualAction(tag);
		actionListModel.addAction(action);
		cardPanel.add(createOptionPanel(name, action, tag.getCompoundTag("options")), name);
		cardLayout.show(cardPanel, name);
		nameField.setText("");
	}
	
	Map<String, ActionOptionManager> actionOptions = new HashMap<String, ActionOptionManager>();
}
