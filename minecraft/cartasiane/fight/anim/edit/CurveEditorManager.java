package cartasiane.fight.anim.edit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cartasiane.fight.anim.AnimAction;
import cartasiane.fight.anim.action.type.PonctualAction;
import cartasiane.fight.anim.action.type.ZoneAction;

public class CurveEditorManager implements ActionListener, ChangeListener
{
	CurveEditorWindow window;
	
	public CurveEditorManager(CurveEditorWindow window)
	{
		this.window = window;
	}
	
	public void actionPerformed(ActionEvent arg0)
	{
		if(arg0.getSource() == window.actionCombo && window.getSelectedAnimAction() != null && window.actionCombo != null)
		{
			window.getSelectedAnimAction().setAction(window.actionCombo.getSelectedIndex());
			window.cardLayout.show(window.cardPanel, (String)window.actionCombo.getSelectedItem());
			window.repaint();
		}
		if(arg0.getSource() == window.conditionCombo)
		{
			if(window.getSelectedAnimAction() != null)
				window.getSelectedAnimAction().setCondition(window.conditionCombo.getSelectedIndex());
		}
		
		if(arg0.getSource() == window.addActionButton && !window.nameField.getText().equals(""))
		{
			window.addAction();
		}
		
		if(arg0.getSource() == window.deleteActionButton)
		{
			window.actionListModel.removeAction(window.actionList.getSelectedIndex());
			window.actionList.setSelectedIndex(-1);
		}
		
		if(arg0.getSource() == window.nameField)
		{
			window.actionListModel.getAction(window.actionList.getSelectedIndex()).setName(window.nameField.getText());
			window.actionList.repaint();
		}
		
		if(arg0.getSource() == window.actionTypeButton)
		{
			AnimAction action = window.actionListModel.getAction(window.actionList.getSelectedIndex());
			if(action instanceof ZoneAction)
			{
				window.actionListModel.setAction(new PonctualAction(window.actionListModel.getActionName(window.actionList.getSelectedIndex())), window.actionList.getSelectedIndex());
				window.actionTypeButton.setText("Point");
			}
			else if(action instanceof PonctualAction)
			{
				window.actionListModel.setAction(new ZoneAction(), window.actionList.getSelectedIndex());
				window.actionTypeButton.setText("Zone");
			}
		}
	}

	public void stateChanged(ChangeEvent arg0)
	{
		if(window.tab.getSelectedIndex() == 2)
			window.curveEditor.actionSelected = true;
		else window.curveEditor.actionSelected = false;
	}
}
