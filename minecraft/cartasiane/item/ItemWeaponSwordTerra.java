package cartasiane.item;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumRarity;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Potion;
import net.minecraft.src.PotionEffect;
import net.minecraft.src.World;

public class ItemWeaponSwordTerra extends ItemWeapon
{
    private int weaponDamage, attackSpeed;
    private float range;

    public ItemWeaponSwordTerra(int id, int damage, int prepareSpeed, int attackSpeed, float range)
    {
        super(id, attackSpeed, prepareSpeed, attackSpeed, range);
        this.setTextureFile("/carta/gui/itemsWeapon.png");
    }

    /**
     * Return an item rarity from EnumRarity
     */
    public EnumRarity getRarity(ItemStack par1ItemStack)
    {
        return par1ItemStack.getItemDamage() == 0 ? EnumRarity.rare : EnumRarity.epic;
    }
    
    
    /**
     * Returns the damage against a given entity.
     */
    public int getDamageVsEntity(Entity par1Entity)
    {
    	((EntityLiving)par1Entity).addPotionEffect(new PotionEffect(Potion.poison.id, 7 * 20, 0));
        return this.weaponDamage;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }
}
