package cartasiane.item;

import java.util.List;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumRarity;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Potion;
import net.minecraft.src.PotionEffect;
import net.minecraft.src.World;

public class ItemFoodRedSpice extends ItemFoodCarta
{
    public ItemFoodRedSpice(int id, int healAmount, float saturationModifier, boolean wolfFood)
    {
        super(id, healAmount, saturationModifier, wolfFood);
        this.setHasSubtypes(true);
        this.setAlwaysEdible();
        this.setTextureFile("/carta/gui/itemsFood.png");
    }
   
    
    /**
     * Return an item rarity from EnumRarity
     */
    public EnumRarity getRarity(ItemStack par1ItemStack)
    {
        return par1ItemStack.getItemDamage() == 0 ? EnumRarity.rare : EnumRarity.epic;
    }

    protected void func_77849_c(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par1ItemStack.getItemDamage() > 0)
        {
            if (!par2World.isRemote)
            {
                par3EntityPlayer.setFire(3);
            }
        }
        else
        {
            super.func_77849_c(par1ItemStack, par2World, par3EntityPlayer);
        }
    }
    /**
     * returns a list of items with the same ID, but different meta (eg: dye returns 16 items)
     */
    public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
    {
      //  par3List.add(new ItemStack(par1, 1, 0));
        par3List.add(new ItemStack(par1, 1, 1));
    }
}
