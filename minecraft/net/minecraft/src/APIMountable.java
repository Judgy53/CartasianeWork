package net.minecraft.src;

import java.util.List;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Material;
import net.minecraft.src.World;
public class APIMountable extends Block
{
	public APIMountable(int x, Material material)
	{
		super(x, material);
	}
	public APIMountable(int x, int y, Material material)
	{
		super(x, y, material);
	}
	@Override
	public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, int par6, float par7, float par8, float par9)
    {
		return onBlockActivated(world, i, j, k, entityplayer, 0.5F, 1.0F, 0.5F, 0, 0, 0, 0);
    }
	public static boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, float y)
    {
		return onBlockActivated(world, i, j, k, entityplayer, 0.5F, y, 0.5F, 0, 0, 0, 0);
    }
	public static boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, float x, float y, float z, int north, int south, int east, int west)
    {
		if (!world.isRemote)
		{
			List<APIMountableEntity> listEMB = world.getEntitiesWithinAABB(APIMountableEntity.class, AxisAlignedBB.getBoundingBox(i, j, k, i + 1.0D, j + 1.0D, k + 1.0D).expand(1D, 1D, 1D));
	    	for (APIMountableEntity entitytocheck : listEMB)
	    	{
	    		if (entitytocheck.orgBlockPosX == i && entitytocheck.orgBlockPosY == j && entitytocheck.orgBlockPosZ == k)
	    		{
	    			entitytocheck.interact(entityplayer);
	    			return true;
	    		}
	    	}
			float mountingX = i + x;
			float mountingY = j + y;
			float mountingZ = k + z;
			if(north != south) 
			{
				int md = world.getBlockMetadata(i, j, k);
				if (md == east) 
				{
					mountingX = i + 1 - z; 
					mountingZ = k + x; 
				}
				else if (md == south) 
				{
					mountingX = i + 1 - x; 
					mountingZ = k + 1 - z; 
				}
				else if (md == west) 
				{
					mountingX = i + z; 
					mountingZ = k + 1 - x; 
				}
			}
	    
	    	APIMountableEntity nemb = new APIMountableEntity(world, entityplayer, i, j, k, mountingX, mountingY, mountingZ); 
	    	world.spawnEntityInWorld(nemb);
	    	nemb.interact(entityplayer);
	    	return true;
		}
		return true;
    }
	
}