package net.minecraft.src;

import org.lwjgl.opengl.GL11;

import cartasiane.misc.Tips;

import net.minecraft.client.Minecraft;

public class GuiConnecting extends GuiScreen
{
    /** A reference to the NetClientHandler. */
    private NetClientHandler clientHandler;

    /** True if the connection attempt has been cancelled. */
    private boolean cancelled = false;
    
    public int cooldownTimer;
    public String host;
    public int port;
    public boolean canLogin;
    public boolean isLogging = true;
    public String tip = (new Tips()).getRandomTip();

    public GuiConnecting(Minecraft par1Minecraft, ServerData par2ServerData)
    {
        this.mc = par1Minecraft;
        ServerAddress var3 = ServerAddress.func_78860_a(par2ServerData.serverIP);
        par1Minecraft.loadWorld((WorldClient)null);
        par1Minecraft.setServerData(par2ServerData);
        this.host = var3.getIP();
        this.port = var3.getPort();
    }

    public GuiConnecting(Minecraft par1Minecraft, String par2Str, int par3)
    {
        this.mc = par1Minecraft;
        par1Minecraft.loadWorld((WorldClient)null);
        this.host = par2Str;
        this.port = par3;
    }

    private void spawnNewServerThread(String par1Str, int par2)
    {
        System.out.println("Connecting to " + par1Str + ", " + par2);
        (new ThreadConnectToServer(this, par1Str, par2)).start();
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
    	super.updateScreen();
    	
    	cooldownTimer++;
    	
        if (this.clientHandler != null)
        {
            this.clientHandler.processReadPackets();
        }
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    protected void keyTyped(char par1, int par2) {}

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        StringTranslate var1 = StringTranslate.getInstance();
        this.controlList.clear();
        this.controlList.add(new GuiButton(0, width - 100, height - 40, 80, 20, var1.translateKey("gui.cancel")));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton par1GuiButton)
    {
        if (par1GuiButton.id == 0)
        {
            this.cancelled = true;

            if (this.clientHandler != null)
            {
                this.clientHandler.disconnect();
            }

            this.mc.displayGuiScreen(new GuiMainMenu());
        }
    }
    
    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        drawDefaultBackground();
        
        int k = mc.renderEngine.getTexture("/carta/fonds/sundown.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(k);
        drawOverTexturedModalRect(width/2 - 2000/4, height/2 - 1200/4, 0, 0, 2000, 1200);
        
        drawRect(0, height - 60, width, height, 0x99000000);
        drawRect(0, 0, width, 25, 0x99000000);
        drawRect(width/2 - 100, height - 40, width/ 2 + 100, height - 20, 553648127);
        drawRect(width/2 - 130, height - 70, width/2 + 130, height - 60, 0x99000000);
        
        String q = "\2477Le saviez-vous ?\247f ";
        fontRenderer.drawString(q + tip, width/2 - (fontRenderer.getStringWidth(q + tip)/2), 9, 0xffffffff);
        
        if(cooldownTimer < 100)
        {
            drawCenteredString(fontRenderer, "Le monde de Cartasiane se charge...", width / 2, height - 60, 0xffffff);
        }
        else if(cooldownTimer >= 100)
        {        	
        	if(canLogin)
        	{
                drawCenteredString(fontRenderer, "Authentification...", width / 2, height - 60, 0xffffff);
        	}
        	else
        	{
                drawCenteredString(fontRenderer, "Une erreur s'est produite...", width / 2, height - 60, 0xffffff);
        	}
        }
        
        if(cooldownTimer >= 98)
        {
            if(isLogging)
            {
            	spawnNewServerThread(host, port);
                canLogin = true;
            	isLogging = false;
            }
        }
        
        if(cooldownTimer >= 100)
        {
            drawRect(width/2 - 98, height - 38, width/2 + 98, height - 22, -1727987968);
            fontRenderer.drawString("100 %", width/2 - (fontRenderer.getStringWidth("100 %")/2), height - 34, 0xffffffff);
        }
        else
        {
            drawRect(width/2 - 98, height - 38, width/2 - 98 + cooldownTimer*2 - 2, height - 22, -1727987968);
            fontRenderer.drawString(cooldownTimer+" %", width/2 - (fontRenderer.getStringWidth(cooldownTimer+" %")/2), height - 34, 0xffffffff);
        }
        
        super.drawScreen(par1, par2, par3);
    }

    /**
     * Sets the NetClientHandler.
     */
    static NetClientHandler setNetClientHandler(GuiConnecting par0GuiConnecting, NetClientHandler par1NetClientHandler)
    {
        return par0GuiConnecting.clientHandler = par1NetClientHandler;
    }

    static Minecraft func_74256_a(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.mc;
    }

    static boolean isCancelled(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.cancelled;
    }

    static Minecraft func_74254_c(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.mc;
    }

    /**
     * Gets the NetClientHandler.
     */
    static NetClientHandler getNetClientHandler(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.clientHandler;
    }

    static Minecraft func_74249_e(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.mc;
    }

    static Minecraft func_74250_f(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.mc;
    }

    static Minecraft func_74251_g(GuiConnecting par0GuiConnecting)
    {
        return par0GuiConnecting.mc;
    }
}
