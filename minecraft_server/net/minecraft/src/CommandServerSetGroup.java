package net.minecraft.src;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.MinecraftServer;

public class CommandServerSetGroup extends CommandBase
{
	String groupes = "\247fGroups: \2474admin\247f, \2479staff\247f, \247cmodo\247f, player";
	String usage = "/setgroup <group> <player>\n"+groupes;
	
    public String getCommandName()
    {
        return "setgroup";
    }

    public String getCommandUsage(ICommandSender par1ICommandSender)
    {
        return usage;
    }

    public void processCommand(ICommandSender par1ICommandSender, String[] par2ArrayOfStr)
    {
        if (par2ArrayOfStr.length > 1)
        {
            if (par2ArrayOfStr[0].equals("admin"))
            {
                if (!par2ArrayOfStr[1].isEmpty())
                {
                	getCommandSenderAsPlayer(par1ICommandSender).addChatMessage("\247cPour ajouter un joueur au groupe des admins, utilisez la commande /op <joueur>");
                }

                return;
            }
        	
            if (par2ArrayOfStr[0].equals("staff"))
            {
                if (!par2ArrayOfStr[1].isEmpty())
                {
                	MinecraftServer.getServer().getConfigurationManager().removeOp(par2ArrayOfStr[1]);
                	MinecraftServer.getServer().getConfigurationManager().removeModo(par2ArrayOfStr[1]);
                	
                	MinecraftServer.getServer().getConfigurationManager().addStaff(par2ArrayOfStr[1]);
                }

                notifyAdmins(par1ICommandSender, getCommandSenderAsPlayer(par1ICommandSender).username+" a ajout� le joueur \""+par2ArrayOfStr[1]+"\" au staff.", new Object[0]);
                return;
            }

            if (par2ArrayOfStr[0].equals("modo"))
            {
                if (!par2ArrayOfStr[1].isEmpty())
                {
                	MinecraftServer.getServer().getConfigurationManager().removeOp(par2ArrayOfStr[1]);
                	MinecraftServer.getServer().getConfigurationManager().removeStaff(par2ArrayOfStr[1]);
                	
                	MinecraftServer.getServer().getConfigurationManager().addModo(par2ArrayOfStr[1]);
                }

                notifyAdmins(par1ICommandSender, getCommandSenderAsPlayer(par1ICommandSender).username+" a ajout� le joueur \""+par2ArrayOfStr[1]+"\" au mod�rateurs.", new Object[0]);
                return;
            }
            
            if (par2ArrayOfStr[0].equals("player"))
            {
                if (!par2ArrayOfStr[1].isEmpty())
                {
                	MinecraftServer.getServer().getConfigurationManager().removeOp(par2ArrayOfStr[1]);
                	MinecraftServer.getServer().getConfigurationManager().removeStaff(par2ArrayOfStr[1]);              	
                	MinecraftServer.getServer().getConfigurationManager().removeModo(par2ArrayOfStr[1]);
                }

                notifyAdmins(par1ICommandSender, getCommandSenderAsPlayer(par1ICommandSender).username+" a retir� le joueur \""+par2ArrayOfStr[1]+"\" de tout les groupes.", new Object[0]);
                return;
            }
        }

        throw new WrongUsageException(usage, new Object[0]);
    }
}