package cartasiane.fight.anim.edit;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class ParryOptionPanel implements AnimOptionPanel, ChangeListener
{
	public JSpinner spinner;
	private JPanel panel;
	private AnimAction action;
	private int efficiency;
	public ParryOptionPanel(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		efficiency = tag.getInteger("efficiency");
		if(spinner != null)
			spinner.setValue(efficiency);
	}
	
	public JPanel getPanel()
	{
		return panel;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("efficiency", efficiency);
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		panel.add(new JTextArea("Efficacité (en %)"));
		spinner = new JSpinner();
		spinner.setValue(efficiency);
		spinner.addChangeListener(this);
		panel.add(spinner);
	}

	public void stateChanged(ChangeEvent arg0)
	{
		efficiency = (Integer) spinner.getValue();
	}

}
