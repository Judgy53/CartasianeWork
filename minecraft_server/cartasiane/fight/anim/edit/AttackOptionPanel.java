package cartasiane.fight.anim.edit;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class AttackOptionPanel implements AnimOptionPanel, ChangeListener
{
	public JSpinner damageSpinner;
	int damage;
	private JPanel panel;
	private AnimAction action;
	public AttackOptionPanel(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		damage = tag.getInteger("damage");
		if(damageSpinner != null)
			damageSpinner.setValue(damage);
	}

	public void initPanel()
	{
		panel = new JPanel();
		panel.add(new JTextArea("D�gats (en %)"));
		damageSpinner = new JSpinner();
		damageSpinner.addChangeListener(this);
		if(action.getActionID() == 0)
		{
			damageSpinner.setValue(damage);
		}
		else damageSpinner.setValue(100);
		panel.add(damageSpinner);
	}
	
	public JPanel getPanel()
	{
		return panel;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("damage", damage);
		return tag;
	}

	public void stateChanged(ChangeEvent arg0)
	{
		damage = (Integer)damageSpinner.getValue();
	}

}
