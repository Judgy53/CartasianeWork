package cartasiane.fight.anim.edit;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class AnimSwitchPanel implements AnimOptionPanel, CaretListener
{
	private JTextArea nameArea;
	private JPanel panel;
	private AnimAction action;
	String name;
	
	public AnimSwitchPanel(AnimAction action)
	{
		this.action = action;
		name = "";
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		name = tag.getString("name");
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setString("name", nameArea.getText());
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		nameArea = new JTextArea(name);
		nameArea.setPreferredSize(new Dimension(60, 16));
		panel.add(new JLabel("Nom de l'animation : "));
		panel.add(nameArea);
		nameArea.addCaretListener(this);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void caretUpdate(CaretEvent arg0)
	{
		name = nameArea.getText();
	}

}
