package cartasiane.fight.anim.edit;

import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.ChangeListener;

import net.minecraft.src.NBTTagCompound;

public interface AnimOptionPanel
{
	public void loadFromNBT(NBTTagCompound tag);
	public NBTTagCompound saveToNBT();
	
	public void initPanel();
	public JPanel getPanel();
}
