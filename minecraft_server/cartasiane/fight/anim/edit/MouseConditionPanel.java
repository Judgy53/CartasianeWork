package cartasiane.fight.anim.edit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class MouseConditionPanel implements AnimOptionPanel, ActionListener
{
	public JComboBox buttonCombo, stateCombo;
	private JPanel panel;
	private AnimAction action;
	
	private int state, button;
	
	public MouseConditionPanel(AnimAction action)
	{
		this.action = action;
	}
	
	public void loadFromNBT(NBTTagCompound tag)
	{
		state = tag.getInteger("state");
		button = tag.getInteger("button");
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("state", state);
		tag.setInteger("button", button);
		return tag;
	}

	public void initPanel()
	{
		panel = new JPanel();
		String ids[] = {"Gauche", "Milieu", "Droite"};
		buttonCombo = new JComboBox(ids);
		String actions[] = {"Up", "Down", "Clic"};
		stateCombo = new JComboBox(actions);
		buttonCombo.addActionListener(this);
		stateCombo.addActionListener(this);
		buttonCombo.setSelectedIndex(button);
		stateCombo.setSelectedIndex(state);
		panel.add(new JTextArea("Bouton "));
		panel.add(buttonCombo);
		panel.add(stateCombo);
	}

	public JPanel getPanel()
	{
		return panel;
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == buttonCombo)
			button = buttonCombo.getSelectedIndex();
		else
			state = stateCombo.getSelectedIndex();
	}

}
