package cartasiane.fight.anim.edit;

import java.util.Collection;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import cartasiane.fight.anim.AnimAction;

public class ActionList implements ListModel
{
	private Vector<AnimAction> actions = new Vector<AnimAction>();
	private Vector<ListDataListener> listeners = new Vector<ListDataListener>();

	public void addListDataListener(ListDataListener arg0)
	{
		listeners.add(arg0);
	}
	
	public void addAction(AnimAction action)
	{
		actions.add(action);
		for(ListDataListener listener : listeners)
			listener.contentsChanged(new ListDataEvent(this, 0, 0, 0));
	}
	
	public void setActions(Collection<AnimAction> actions)
	{
		this.actions = new Vector<AnimAction>();
		for(AnimAction action : actions)
			this.actions.add(action);
		for(ListDataListener listener : listeners)
			listener.contentsChanged(new ListDataEvent(this, 0, 0, 0));
	}
	
	public void setAction(AnimAction action, int id)
	{
		actions.set(id, action);
	}
	
	public void reset()
	{
		actions = new Vector<AnimAction>();
	}
	
	public void removeAction(int id)
	{
		actions.remove(id);
		for(ListDataListener listener : listeners)
			listener.contentsChanged(new ListDataEvent(this, 0, 0, 0));
	}

	public Object getElementAt(int arg0)
	{
		return actions.get(arg0).getName();
	}
	
	public AnimAction getAction(int id)
	{
		return actions.get(id);
	}

	public int getSize()
	{
		return actions.size();
	}
	
	void insertAction(int id, AnimAction action)
	{
		actions.insertElementAt(action, id);
		for(ListDataListener listener : listeners)
			listener.contentsChanged(new ListDataEvent(this, 0, 0, 0));
	}

	public void removeListDataListener(ListDataListener arg0)
	{
		listeners.remove(arg0);
	}
	
	public String getActionName(int i)
	{
		return actions.get(i).getName();
	}

	public Vector<AnimAction> getActionList() {
		return actions;
	}

	public void clear()
	{
		actions = new Vector<AnimAction>();
	}
}
