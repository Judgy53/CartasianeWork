package cartasiane.fight.anim.edit;

import javax.swing.JPanel;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class EmptyConditionPanel implements AnimOptionPanel
{
	public EmptyConditionPanel(AnimAction action)
	{
		
	}

	public void loadFromNBT(NBTTagCompound tag)
	{
		
	}

	public NBTTagCompound saveToNBT()
	{
		return new NBTTagCompound();
	}

	public void initPanel()
	{
		
	}

	public JPanel getPanel()
	{
		return new JPanel();
	}

}
