package cartasiane.fight.anim;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import cartasiane.fight.anim.action.PonctualAction;
import cartasiane.fight.anim.action.ZoneAction;
import cartasiane.util.BezierCurve;

public class AnimFactory
{
	public AnimFactory(NBTTagCompound tag)
	{
		id = tag.getInteger("id");
		NBTTagList list = tag.getTagList("curves");
		if(list == null)
		{
			curves = new BezierCurve[36];
			for(int i=0; i<36; i++)
			{
				curves[i] = new BezierCurve();
			}
		}
		else
		{
			curves = new BezierCurve[list.tagCount()];
			for(int i=0; i<list.tagCount(); i++)
				curves[i] = new BezierCurve((NBTTagList)list.tagAt(i));
		}
		NBTTagList actionList = tag.getTagList("actions");
		if(actionList != null)
		{
			actions = new AnimAction[actionList.tagCount()];
			for(int i=0; i<actionList.tagCount(); i++)
			{
				setAction((NBTTagCompound)actionList.tagAt(i), i);
			}
		}
	}
	
	public void setAction(NBTTagCompound tag, int i)
	{
		String name = tag.getString("Name");
		AnimAction action;
		if(tag.getInteger("type") == 0)
			action = new PonctualAction(tag);
		else action = new ZoneAction(tag);
		actions[i] = action;
	}
	
	public Anim makeAnim(EntityPlayer player, int length, boolean left)
	{
		return new Anim(player, length, left, curves, actions);
	}
	
	public Integer getId()
	{
		return id;
	}
	
	int id;

	BezierCurve[] curves;
	AnimAction[] actions;
}
