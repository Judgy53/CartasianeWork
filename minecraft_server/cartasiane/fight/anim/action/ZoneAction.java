package cartasiane.fight.anim.action;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class ZoneAction extends AnimAction
{
	float x1, x2;

	public ZoneAction()
	{

	}

	public ZoneAction(String name, int condition, int action)
	{
		super(name);
		conditionID = condition;
		actionID = action;
	}

	public ZoneAction(NBTTagCompound tag)
	{
		super(tag);
		x1 = tag.getFloat("x1");
		x2 = tag.getFloat("x2");
	}

	public void onClick(int button, float x, float y)
	{
		x1 = x;
		x2 = x;
	}

	public void onMouseReleased(int button, float x, float y)
	{

	}

	public void onMouseMove(int button, float x, float y)
	{
		x2 = x;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("type", 1);
		tag.setFloat("x1", x1);
		tag.setFloat("x2", x2);
		return tag;
	}
	
	public boolean isInside(int t, int length)
	{
		if(x1 == x2)
			return false;
		return ((float)t/length >= x1) && ((float)t/length <= x2);
	}
}