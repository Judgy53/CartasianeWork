package cartasiane.fight.anim.action;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.AnimAction;

public class PonctualAction extends AnimAction
{
	float pos = -10;

	public PonctualAction(String name)
	{
		super(name);
	}

	public PonctualAction(String name, int condition, int action)
	{
		super(name);
		conditionID = condition;
		actionID = action;
	}

	public PonctualAction(NBTTagCompound tag)
	{
		super(tag);
		pos = tag.getFloat("pos");
	}


	public void onClick(int button, float x, float y)
	{
		pos = x;
	}

	public void onMouseReleased(int button, float x, float y)
	{

	}

	public void onMouseMove(int button, float x, float y)
	{
		pos = x;
	}

	public NBTTagCompound saveToNBT()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag.setInteger("type", 0);
		tag.setFloat("pos", pos);
		return tag;
	}

	public boolean isInside(int t, int length)
	{
		return ((float)t/length <= pos) && ((float)(t+1)/length > pos);
	}
}