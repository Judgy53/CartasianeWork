package cartasiane.fight.anim;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.MovingObjectPosition;
import cartasiane.util.BezierCurve;

public class Anim
{
	private boolean left;
	private int length, time, speed;
	protected EntityPlayer player;
	
	private BezierCurve[] curves;
	private AnimAction[] actions;
	private boolean init = false;

	public Anim(EntityPlayer player, int length, boolean left, BezierCurve[] curves, AnimAction[] actions)
	{
		this.player = player;
		this.length = length;
		this.time = 0;
		this.speed = 1;
		this.left = left;
		this.curves = curves;
		this.actions = actions;
	}
	
	protected EntityPlayer getPlayer()
	{
		return player;
	}
	
	public Anim restart()
	{
		time = 0;
		return this;
	}
	
	public void anim()
	{
		time += speed;
		onUpdate();
		if(time >= length)
			player.setAnimation(null, 0, false);
		MovingObjectPosition pos;
		for(AnimAction action : actions)
		{
			if(action.isInside(time, length))
			{
				System.out.println(action.name);
			}
		}
	}
	
	public float ratio()
	{
		return time/(float)length;
	}
	
	protected boolean isAtFrame(float ratio)
	{
		return ratio() < ratio && (time+1)/(float)length >= ratio;
	}
	
	public boolean isLeft()
	{
		return left;
	}
	
	public int getLength()
	{
		return length;
	}

	protected void onAnimFinished()
	{
		
	}
	
	public void onClick(boolean left)
	{
		
	}
	
	public void onUpdate()
	{
		
	}

	public float calc(int curve)
	{
		return curves[curve].getFunction().calc(ratio());
	}

	public void setTime(int time)
	{
		this.time = time;
	}
}
