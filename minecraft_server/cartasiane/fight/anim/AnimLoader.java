package cartasiane.fight.anim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.server.MinecraftServer;
import net.minecraft.src.CompressedStreamTools;
import net.minecraft.src.NBTTagCompound;

public class AnimLoader
{
	private static File animDirectory;
	Map<Integer, AnimFactory> anims = new HashMap<Integer, AnimFactory>();
	Map<String, Integer> names = new HashMap<String, Integer>();
	public static void setDirectory()
	{
		animDirectory = MinecraftServer.getServer().getFile("/anims/");
	}
	
	public AnimLoader()
	{
		File files[] = animDirectory.listFiles();
		if(files != null)
		for(File file : files)
		{
			if(file.getName().endsWith(".dat"))
			{
		 		loadAnim(file);
			}
		}
	}
	
	public NBTTagCompound loadAnim(String name)
	{
		File file = new File(animDirectory, name+".dat");
        try
        {
			return (NBTTagCompound)CompressedStreamTools.readCompressed(new FileInputStream(file)).getTag("main");
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
        return null;
	}
	
	public AnimFactory loadAnim(File file)
	{
        try 
        {
        	NBTTagCompound list = (NBTTagCompound)CompressedStreamTools.readCompressed(new FileInputStream(file)).getTag("main");
        	if(list != null)
        	{
        		
        		AnimFactory anim = new AnimFactory(list);
        		anims.put(anim.getId(), anim);
        	}
        	else return null;
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
        return null;
	}
	
	public AnimFactory getAnim(String name)
	{
		return this.anims.get(names.get(name));
	}

	public AnimFactory getAnim(int animId)
	{
		return anims.get(animId);
	}

}
