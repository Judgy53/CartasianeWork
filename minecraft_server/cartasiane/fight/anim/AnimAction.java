package cartasiane.fight.anim;

import java.util.Map;

import net.minecraft.src.NBTTagCompound;
import cartasiane.fight.anim.edit.ActionOptionManager;
import cartasiane.fight.anim.edit.AnimOptionPanel;

public abstract class AnimAction
{
	protected int conditionID, actionID;
	protected String name;
	protected AnimOptionPanel condition;
	
	public AnimAction(String name)
	{
		this.name = name;
	}
	
	public AnimAction()
	{
		
	}
	
	public AnimAction(NBTTagCompound tag)
	{
		name = tag.getString("Name");
		NBTTagCompound optionTag = tag.getCompoundTag("options");
		conditionID = optionTag.getInteger("condition");
		actionID = optionTag.getInteger("action");
	}
	
	public abstract void onClick(int button, float x, float y);
	public abstract void onMouseReleased(int button, float x, float y);
	public abstract void onMouseMove(int button, float x, float y);
	public int getCondition()
	{
		return conditionID;
	}
	
	public int getActionID()
	{
		return actionID;
	}
	
	public void setAction(int action)
	{
		actionID = action;
	}
	
	public void setCondition(int condition)
	{
		conditionID = condition;
	}
	
	public NBTTagCompound toNBT(Map<String, ActionOptionManager> actionOptions)
	{
		NBTTagCompound tag = saveToNBT();
		tag.setString("Name", name);
		tag.setCompoundTag("options", actionOptions.get(name).saveToNBT());
		return tag;
	}
	
	public String getName()
	{
		return name;
	}
	
	public abstract NBTTagCompound saveToNBT();

	public void setName(String text)
	{
		this.name = text;
	}
	
	public abstract boolean isInside(int t, int length);
}
