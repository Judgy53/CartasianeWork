package cartasiane.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.NetHandler;
import net.minecraft.src.Packet;

public class Packet157StartAnim extends Packet
{
	public int playerId, animId, length, time;
	public boolean left;
	
	public Packet157StartAnim()
	{
		
	}
	
	public Packet157StartAnim(EntityPlayer player, int animId, int length, int time, boolean left)
	{
		this.playerId = player.entityId;
		this.animId = animId;
		this.length = length;
		this.time = time;
		this.left = left;
	}
	
	public void readPacketData(DataInputStream var1) throws IOException
	{
		playerId = var1.readInt();
		animId = var1.readInt();
		length = var1.readInt();
		time = var1.readInt();
		left = var1.readBoolean();
	}

	public void writePacketData(DataOutputStream var1) throws IOException
	{
		var1.writeInt(playerId);
		var1.writeInt(animId);
		var1.writeInt(length);
		var1.writeInt(time);
		var1.writeBoolean(left);
	}

	public void processPacket(NetHandler var1)
	{
		var1.handleStartAnim(this);
	}

	public int getPacketSize()
	{
		return 17;
	}

}
