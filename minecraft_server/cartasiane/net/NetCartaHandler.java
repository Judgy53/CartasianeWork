package cartasiane.net;

import net.minecraft.server.MinecraftServer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerMP;
import net.minecraft.src.INetworkManager;
import net.minecraft.src.NetServerHandler;
import net.minecraft.src.WorldServer;

public class NetCartaHandler extends NetServerHandler
{

	public NetCartaHandler(MinecraftServer par1, INetworkManager par2,
			EntityPlayerMP par3) {
		super(par1, par2, par3);
	}

	public void handleStartAnim(Packet157StartAnim packet)
	{
		WorldServer world = this.mcServer.worldServerForDimension(this.playerEntity.dimension);
		EntityPlayer player = (EntityPlayer)world.getEntityByID(packet.playerId);
		player.setAnimation(mcServer.animLoader.getAnim(packet.animId), packet.length, packet.left);
		player.setAnimationTime(packet.time);
		world.getEntityTracker().sendPacketToTrackedPlayers(player, packet);
	}
}
