package cartasiane.blocs;

import java.util.List;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IBlockAccess;
import net.minecraft.src.Material;
import net.minecraft.src.MathHelper;
import net.minecraft.src.World;

public class BlockChaiseF extends Block
{
	private final Block modelBlock;
	public BlockChaiseF(int par1, Block par2Block, int par3)
	{
		super(par1, par2Block.blockMaterial);
		modelBlock = par2Block;
		this.blockIndexInTexture = par3;
		this.setCreativeTab(CreativeTabs.tabDecorations);
	}
	public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity)
	{
		this.modelBlock.onEntityWalking(par1World, par2, par3, par4, par5Entity);
	}
	/**
	 * Il n'est pas Opaque > Return false
	 */
	public boolean isOpaqueCube() {
		return false;
	}
	/**
	 * Notre bloc n'est pas un bloc normal, alors return false
	 */
	public boolean renderAsNormalBlock() {
		return false;
	}
	/**
	 * Selection du render du bloc.
	 */
	public int getRenderType() {
		return 60;
	}
	/**
	 * 
	 * Mise � jour du metadata.
	 */
	public void updateBlockMetadata(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8)
	{
		if (par5 == 0 || par5 != 1 && (double)par7 > 0.5D)
		{
			int data = par1World.getBlockMetadata(par2, par3, par4);
			par1World.setBlockMetadataWithNotify(par2, par3, par4, data | 3);
		}
	}
	/**
	 * Quand le bloc est plac�
	 */
	public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving)
	{
		int var6 = MathHelper.floor_double((double)(par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		int var7 = par1World.getBlockMetadata(par2, par3, par4) & 4;

		if (var6 == 0)
		{
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 2 | var7);
		}

		if (var6 == 1)
		{
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 1 | var7);
		}

		if (var6 == 2)
		{
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 3 | var7);
		}

		if (var6 == 3)
		{
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 0 | var7);
		}
	}

	/**
	 * Add du bloc.
	 */
	public void onBlockAdded(World par1World, int par2, int par3, int par4)
	{
		par1World.setBlockMetadataWithNotify(par2, par3, par4, 1);
	}    
	/**
	 * Tra�age de la HitBox
	 */
	public void setBlockBoundsBasedOnState(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
	{
		this.setBlockBounds(0.2F, 0F, 0.1F, 0.9F, 1.5F, 0.9F);
		//  this.setBlockBounds(0.8F, 0F, 0.1F, 0.9F, 1.5F, 0.9F);

	}
	/**
	 * Fonction pour la collision.
	 */
	public void addCollidingBlockToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
	{
		int var1 = par1World.getBlockMetadata(par2, par3, par4); // R�cup�ration du Meta data
		if (var1 == 0) {
			this.setBlockBounds(0.8F, 0F, 0.1F, 0.9F, 1.5F, 0.9F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			this.setBlockBounds(0F,0F,0F, 1F,0.7F,1F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		} else if(var1 == 1) {
			this.setBlockBounds(0.13F, 0.0F, 0.020F, 0.87F, 0.75F, 0.99F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			this.setBlockBounds(0.2F, 0F, 0.1F, 0.3F, 1.5F, 0.9F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		}
		else if(var1 == 2) {
			this.setBlockBounds(0.2F, 0F, 0.8F, 0.9F, 1.5F, 0.9F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			this.setBlockBounds(0F,0F,0F, 1F,0.7F,1F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		} else if(var1 == 3) {
			this.setBlockBounds(0.2F, 0F, 0.1F, 0.9F, 1.5F, 0.2F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			this.setBlockBounds(0F,0F,0F, 1F,0.7F,1F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		}

	}

	@Override
	public boolean canPlaceBlockAt(World par1World, int par2, int par3, int par4) {

		return super.canPlaceBlockAt(par1World, par2, par3, par4);
	}

	/**
	 * Plus de bug texture si un bloc est poser � coter.
	 */
	 public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
	    {
	    return true;
	    }


}
