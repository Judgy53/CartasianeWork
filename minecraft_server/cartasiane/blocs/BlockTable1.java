package cartasiane.blocs;

import java.util.List;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.IBlockAccess;
import net.minecraft.src.MathHelper;
import net.minecraft.src.World;

public class BlockTable1 extends Block
{
	private final Block modelBlock;
	public BlockTable1(int par1, Block par2Block, int par3) {
		super(par1, par2Block.blockMaterial);
		this.setCreativeTab(CreativeTabs.tabAllSearch );
	     this.blockIndexInTexture = par3;
	     modelBlock = par2Block;
	}
	 public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity)
	    {
	        this.modelBlock.onEntityWalking(par1World, par2, par3, par4, par5Entity);
	    }
	public boolean isOpaqueCube() {
		return false;
	}
	 public void updateBlockMetadata(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8)
	    {
	    	if (par5 == 0 || par5 != 1 && (double)par7 > 0.5D)
	    	{
	    		int data = par1World.getBlockMetadata(par2, par3, par4);
	    		par1World.setBlockMetadataWithNotify(par2, par3, par4, data | 3);
	    	}
	    }
	 /**
	  * Quand le bloc est plac�
	  */
	  public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving)
	    {
	        int var6 = MathHelper.floor_double((double)(par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
	        int var7 = par1World.getBlockMetadata(par2, par3, par4) & 4;

	        if (var6 == 0)
	        {
	            par1World.setBlockMetadataWithNotify(par2, par3, par4, 2 | var7);
	        }

	        if (var6 == 1)
	        {
	            par1World.setBlockMetadataWithNotify(par2, par3, par4, 1 | var7);
	        }

	        if (var6 == 2)
	        {
	            par1World.setBlockMetadataWithNotify(par2, par3, par4, 3 | var7);
	        }

	        if (var6 == 3)
	        {
	            par1World.setBlockMetadataWithNotify(par2, par3, par4, 0 | var7);
	        }
	    }
	public int getRenderType() {
		return 61;
	}
	public boolean renderAsNormalBlock() {
		return false;
	}
	 public void onBlockAdded(World par1World, int par2, int par3, int par4)
	    {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, 1);
	    }    
	 public void setBlockBoundsBasedOnState(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
	    {
		 this.setBlockBounds(00F, 0F, 0.0F, 1.0F, 1.0F, 1.0F);
		//  this.setBlockBounds(0.8F, 0F, 0.1F, 0.9F, 1.5F, 0.9F);  	
	    }
	 public void addCollidingBlockToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
	 {
		 this.setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
		 super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
	 }
	 
	 /**
		 * Plus de bug texture si un bloc est poser � coter.
		 */
		 public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
		    {
		    return true;
		    }

}
