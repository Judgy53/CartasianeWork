package cartasiane.blocs;

import java.util.List;

import net.minecraft.src.APIMountable;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.IBlockAccess;
import net.minecraft.src.MathHelper;
import net.minecraft.src.World;

public class BlockBanc extends APIMountable
{
	private final Block mdl;
	public BlockBanc(int par1, Block par2Block, int par3) 
	{
		super(par1, par2Block.blockMaterial);
		this.mdl = par2Block;
		this.blockIndexInTexture = par3;
		this.setCreativeTab(CreativeTabs.tabAllSearch);
	}
	public int getRenderType()
	{
		return 76;
	}
	public boolean isOpaqueCube() 
	{
		return false;
	}
	public boolean renderAsNormalBlock() 
	{
		return false;
	}
	 public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
	 
	 {
	  
	 return true;
	  
	 }
		public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving)
		{
			int var6 = MathHelper.floor_double((double)(par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
			int var7 = par1World.getBlockMetadata(par2, par3, par4) & 4;

			if (var6 == 0)
			{
				par1World.setBlockMetadataWithNotify(par2, par3, par4, 2 | var7);
			}
			

			if (var6 == 1)
			{
				par1World.setBlockMetadataWithNotify(par2, par3, par4, 1 | var7);
			}

			if (var6 == 2)
			{
				par1World.setBlockMetadataWithNotify(par2, par3, par4, 3 | var7);
			}

			if (var6 == 3)
			{
				par1World.setBlockMetadataWithNotify(par2, par3, par4, 0 | var7);
			}
		}
		public void setBlockBoundsBasedOnState(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
		{
			this.setBlockBounds(0.1F, 0F, 0.1F, 0.9F, 0.85F, 0.9F);
		}
		public void addCollidingBlockToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
		{
			int var1 = par1World.getBlockMetadata(par2, par3, par4); // Récupération du Meta data
			if (var1 == 0) {
				this.setBlockBounds(0.2F, 0F, 0.2F, 0.7F, 0.5F,0.8F);
				super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			} else if(var1 == 1) {
			this.setBlockBounds(0.2F, 0F, 0.2F, 0.7F, 0.5F,0.8F);
			super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			}
			else if(var1 == 2) {
				this.setBlockBounds(0.2F, 0F, 0.2F, 0.7F, 0.5F,0.8F);
				super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			
			} else if(var1 == 3) {
				this.setBlockBounds(0.2F, 0F, 0.2F, 0.7F, 0.5F,0.8F);
				super.addCollidingBlockToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
			}

		}

}
