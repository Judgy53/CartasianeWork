package cartasiane.util;

import java.util.Vector;

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;

public class BezierCurve
{
	int selectedPoint = -1;
	
	public BezierCurve()
	{
		this.points = new Vector<AnimPoint>();
		points.add(new AnimPoint(0, 0));
		points.add(new AnimPoint(1, 0));
	}
	
	public BezierCurve(NBTTagList tag)
	{
		this.points = new Vector<AnimPoint>();
		if(tag == null)
		{
			points.add(new AnimPoint(0, 0));
			points.add(new AnimPoint(1, 0));
			return;
		}
		for(int i=0; i<tag.tagCount(); i++)
		{
			points.add(new AnimPoint((NBTTagCompound)(tag.tagAt(i))));
		}
	}
	
	public void selectPoint(float x, float y)
	{
		selectedPoint = -1;
		float relX = x;
		float relY = y;
		for(int i=0; i<points.size(); i++)
		{
			
			float xM = this.points.get(i).time;
			float yM = this.points.get(i).var;
			if((relX-xM)*(relX-xM) + (relY-yM)*(relY-yM) <=  1f/8000)
			{
				selectedPoint = i;
				return;
			}
		}
	}
	
	public void addPoint(float x, float y)
	{
		float relX = x;
		float relY = y;
		int i;
		for(i=0; i+1 < points.size() && relX > points.get(i+1).time; i++);
		if(i+1 == points.size() || i < points.size()-1 && relX == points.get(i+1).time)
			return;
		float xA = this.points.get(i).time, yA = this.points.get(i).var;
		float xB = points.get(i+1).time, yB = points.get(i+1).var;
		float xAB = xB-xA, xAC = relX-xA, yAB = yB-yA, yAC = relY-yA;
		float g = (xAB*xAC+yAB*yAC)/(xAB*xAB + yAB*yAB);

		xAC = (xB-xA)*g; yAC = (yB-yA)*g;
		points.insertElementAt(new AnimPoint(relX, relY), i);
		
		sortPoints(selectedPoint);
	}
	
	public void delPoint(float x, float y)
	{
		selectedPoint = -1;
		for(int i=0; i<points.size(); i++)
		{
			float xM = this.points.get(i).time;
			float yM = this.points.get(i).var;
			if((x-xM)*(x-xM) + (y-yM)*(y-yM) <=  1f/8000)
			{
				points.remove(i);
				return;
			}
		}
	}
	
	public void movePoint(float x, float y)
	{
		float relX = x;
		float relY = y;

		if(this.selectedPoint >= 0)
		{
			AnimPoint pt = points.get(selectedPoint);
				pt.var = relY;
				if(selectedPoint == 0)
					pt.time = 0;
				else if(selectedPoint == points.size()-1)
					pt.time = 1;
				else
					pt.time = relX;
		}
		sortPoints(selectedPoint);
	}
	
	public int sortPoints(int selectedPoint)
	{
		Vector<AnimPoint> points = this.points;
		this.points = new Vector<AnimPoint>();
		boolean selected = false;
		int selectedId = -1;
		for(int i=0; i<points.size(); i++)
		{
			int j;
			for(j=0; j < this.points.size() && points.get(i).time > this.points.get(j).time; j++);
			if(selected && j <= selectedId)
				selectedId++;
			if(i == selectedPoint)
			{
				selected = true;
				selectedId = j;
			}
			this.points.insertElementAt(points.get(i), j);
		}
		if(selected)
			selectedPoint = selectedId;
		return selectedPoint;
	}
	
	public void unselect()
	{
		selectedPoint = -1;
	}
	
	public NBTTagList saveToNBT()
	{
		NBTTagList list = new NBTTagList();
		for(AnimPoint pt : points)
			list.appendTag(pt.saveToNBT());
		return list;
	}
	
	public LinInterp getFunction()
	{
		return new LinInterp(points);
	}
	
	public int countPoints()
	{
		return points.size();
	}
	
	private Vector<AnimPoint> points;

	public Vector<AnimPoint> getPoints()
	{
		return points;
	}
}
