package cartasiane.util;

public abstract class Function
{
	public abstract float calc(float x);
	
}
