package cartasiane.mobs;

import net.minecraft.src.Block;
import net.minecraft.src.DamageSource;
import net.minecraft.src.EntityBlaze;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityThrowable;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.World;

public class EntityMagicFire extends EntityThrowable
{
	public EntityMagicFire(World par1World, double var2, double var4, double var6)
	{
		super(par1World);
	}

	public EntityMagicFire(World par1World, EntityLiving par2EntityLiving)
	{
		super(par1World, par2EntityLiving);
	}

	public EntityMagicFire(World par1World, EntityDemon entityDemon, double par4, double par6)
	{
		super(par1World);
	}

	public int getBrightnessForRender(float par1)
	{
		return 15728880;
	}

	public float getBrightness(float par1)
	{
		return 1.0F;
	}

	protected void onImpact(MovingObjectPosition par1MovingObjectPosition)
	{
		if(par1MovingObjectPosition.entityHit !=null){	

			par1MovingObjectPosition.entityHit.setFire(5);

			this.worldObj.playSoundAtEntity(this, "random.fire", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
			this.setDead();
		}
		else
		{
			int posX = par1MovingObjectPosition.blockX;
			int posY = par1MovingObjectPosition.blockY;
			int posZ = par1MovingObjectPosition.blockZ;
			switch (par1MovingObjectPosition.sideHit)
			{
			case 0:
				--posY;
				break;

			case 1:
				++posY;
				break;
			case 2:
				--posZ;
				break;
			case 3:
				++posZ;
				break;
			case 4:
				--posX;
				break;
			case 5:
				++posX;
			}

			if(this.worldObj.isBlockNormalCube(posX, posY, posZ)) {
				this.worldObj.setBlockWithNotify(posX, posY, posZ, Block.fire.blockID);
				this.setDead();
			}
			else if(this.worldObj.isAirBlock(posX, posY, posZ)) {
				this.worldObj.setBlockWithNotify(posX, posY, posZ, Block.fire.blockID);
				this.setDead();
			}
			else if(this.isInWater()) 
			{
				this.worldObj.spawnParticle("hugeexplosion", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
				this.setDead();
			}
			this.setDead();
		}
	}
	
	public void onUpdate() 
	{
		this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
		this.worldObj.spawnParticle("flame", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
		if(this.ticksExisted == 200) {
			this.setDead();
		}
		super.onUpdate();
	}
}
