package cartasiane.item;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Potion;
import net.minecraft.src.PotionEffect;
import net.minecraft.src.World;

public class ItemFoodRedSpice extends ItemFoodCarta
{
    public ItemFoodRedSpice(int par1, int par2, float par3, boolean par4)
    {
        super(par1, par2, par3, par4);
        this.setHasSubtypes(true);
        this.setAlwaysEdible();
    }
    

    protected void func_77849_c(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par1ItemStack.getItemDamage() > 0)
        {
            if (!par2World.isRemote)
            {
                par3EntityPlayer.setFire(3);
            }
        }
        else
        {
            super.func_77849_c(par1ItemStack, par2World, par3EntityPlayer);
        }
    }
}