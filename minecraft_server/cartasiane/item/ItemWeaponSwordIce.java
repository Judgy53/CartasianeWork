package cartasiane.item;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.World;

public class ItemWeaponSwordIce extends ItemWeapon
{
    private int weaponDamage, attackSpeed;
    private float range;

    public ItemWeaponSwordIce(int id, int damage, int prepareSpeed, int attackSpeed, float range)
    {
        super(id, attackSpeed, prepareSpeed, attackSpeed, range);
    }
    
    /**
     * Returns the damage against a given entity.
     */
    public int getDamageVsEntity(Entity par1Entity)
    {
    	par1Entity.moveEntity(par1Entity.posX - 1, par1Entity.posY, par1Entity.posZ);
        return this.weaponDamage;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }
}
